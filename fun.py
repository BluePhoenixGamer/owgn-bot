import discord
from discord.ext import commands
import asyncio
import random

bot = commands.Bot(command_prefix=commands.when_mentioned_or('&'))
bot.remove_command('help')


class Fun:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def test(self, ctx):
        await self.bot.delete_message(ctx.message)
        bot_test = await self.bot.say("Testing? Testing.....")
        await asyncio.sleep(10)
        await self.bot.delete_message(bot_test)

    @commands.command(pass_context=True)
    async def shrug(self, ctx):
        await self.bot.delete_message(ctx.message)
        await self.bot.say("¯＼(º_o)/¯")

    @commands.command(pass_context=True)
    async def magic8ball(self, ctx, *, question: str = None):
        if question is None:
            await self.bot.delete_message(ctx.message)
            await self.bot.say("I can't answer nothing!")
        else:
            answer = random.choice(["Of course", "Yeah", "Yes", "Why not?", "Ask again later!", "Pretty sure, yes.....",
                                    "Hope so......", "Maybe", "Yes, what could go wrong?", "Don't know",
                                    "Why are you asking me about this?", "Why did you ask",
                                    "I'm not smart enough to answer that dude",
                                    "And why should i answer that?", "I don't know, ask god", "Rather not answer"
                                                                                              "Pay me and i'll answer you....",
                                    "Hope not", "Why should you?", "Hell na'",
                                    "Don't think so", "Nope", "I'm very doubtful", "No", "Don't, just **NO**!!"])
            await self.bot.delete_message(ctx.message)
            await self.bot.say(answer)

    @commands.command(pass_context=True)
    async def echo(self, ctx, *, echo: str):
        await self.bot.delete_message(ctx.message)
        await self.bot.say(echo)

    @commands.command(pass_context=True)
    async def dice(self, ctx):
        await self.bot.delete_message(ctx.message)
        number = random.choice([1, 2, 3, 4, 5, 6])
        await self.bot.say("Rolled a dice... " + str(number))

    @commands.command(pass_context=True)
    async def remind(self, ctx, time: int, *, remember: str = None):
        timem = time * 60
        if time is None:
            await self.bot.say("After how long should i remind you?")
        elif remember is None:
            await self.bot.say("Got it! i will remind you after " + str(time) + " minutes")
            await asyncio.sleep(timem)
            embed = discord.Embed(title="Reminder",
                                  description="It has now gone " + str(time) + " minutes " + ctx.message.author.mention)
            embed.set_author(name="OwGN-Helper",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                      "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            await self.bot.send_message(ctx.message.author, embed=embed)
        else:
            await self.bot.say("Got it! i will remind you after " + str(time) + " minutes")
            await asyncio.sleep(timem)
            embed = discord.Embed(title="Reminder", description="It has now gone " + str(
                time) + " minutes. And you told me to remind you: " + remember)
            embed.set_author(name="OwGN-Helper", url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                      "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            await self.bot.send_message(ctx.message.author, embed=embed)

    @commands.command(pass_context=True)
    async def ami(self, ctx, role = None):
        if role is None:
            return await self.bot.say("Please mention a role or write its name.")
        elif role == int:
            return await self.bot.say("Please mention a role or write its name. You just typed a number.")
        else:
            if role == str:
                if role in [role.name for role in ctx.message.author.roles]:
                    return await self.bot.say("Yes, you have the role: `{}`".format(role))
                else:
                    return await self.bot.say("No, you do not have the role: ``{}".format(role))
            elif role in [member.mention for member in ctx.message.server.members]:
                return await self.bot.say("That is not a role. That is a user.")
            else:
                role_object = discord.utils.get(ctx.message.server.roles, mention=role)
                print("role mention string")
                if role in [role.mention for role in ctx.message.author.roles]:
                    return await self.bot.say("Yes you have the role: {}".format(role_object.name))
                else:
                    return await self.bot.say("No, you do not have the role: {}".format(role_object.name))

    @commands.command(pass_context=True)
    async def insult(self, ctx):
        await self.bot.say(random.choice([
            "{0.author.mention}, Does your ass get jealous of all the shit that comes out of your mouth?".format(
                ctx.message),
            "{0.author.mention}, Oh my God, look at you. Was anyone else hurt in the accident?".format(ctx.message),
            "{0.author.mention}, You're so fat you need cheat codes to play Wii Fit".format(ctx.message),
            "{0.author.mention}, Grasp your ears firmly and remove your head from your ass.".format(ctx.message),
            "{0.author.mention}, If you really want to know about mistakes, you should ask your parents.".format(
                ctx.message),
            "{0.author.mention}, I've seen people like you, but I had to pay admission!".format(ctx.message),
            "{0.author.mention}, You're so fat you show up on radar.".format(ctx.message),
            "{0.author.mention}, You're so fat, you could sell shade.".format(ctx.message),
            "{0.author.mention} Is so old that his blood type was discontinued.".format(ctx.message),
            "{0.author.mention}, You couldn't pour water out of a boot if the instructions were on the heel.".format(
                ctx.message),
            "{0.author.mention}, You're so fat the back of your neck looks like a pack of hot-dogs.".format(
                ctx.message),
            "{0.author.mention}, Did you know they used to be called Jumpolines until your mum jumped on one in 1972?".format(
                ctx.message),
            "{0.author.mention}, How come bleach hasn't worked yet?".format(ctx.message),
            "{0.author.mention} Realised his balls were so small when a scientist couldn't even see them with an microscope".format(
                ctx.message)
        ]))


def setup(bot):
    bot.add_cog(Fun(bot))
