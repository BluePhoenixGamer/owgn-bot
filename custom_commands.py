import asyncio
import sqlite3

import discord
from discord.ext import commands
from RPG import RPG


class CustomCommands:
    def __init__(self, bot):
        self.bot = bot
        self.conn = sqlite3.connect('users.db', isolation_level=None)
        self.c = self.conn.cursor()
        self.conn.execute("ATTACH DATABASE 'rpg.db' AS RPG")
        self.conn.execute("ATTACH DATABASE 'shop.db' AS SHOP")
        self.conn.execute("""CREATE TABLE IF NOT EXISTS Users(
                              UserID TEXT,
                              Coins FLOAT,
                              SecondCoins FLOAT,
                              Xp INTEGER,
                              Online TEXT,
                              Party TEXT,
                              MyGroup TEXT,
                              Guild TEXT,
                              PRIMARY KEY(UserID))""")
        self.conn.execute("""CREATE TABLE IF NOT EXISTS rpg.Inventory(
                              UserID TEXT,
                              Type TEXT,
                              Name TEXT,
                              Quantity INT)""")
        self.conn.execute("""CREATE TABLE IF NOT EXISTS SHOP.Shop(
                              UserID TEXT,
                              Item TEXT,
                              Type TEXT,
                              Price FLOAT,
                              Sold TEXT)""")
        global instance
        instance = bot

    def create_user_if_not_exists(self, user_id):
        res = self.conn.execute("SELECT COUNT(*) FROM Users WHERE UserID=?", (user_id,))
        user_count = res.fetchone()[0]
        if user_count < 1:
            print("Creating user with id " + user_id)
            self.conn.execute("INSERT INTO Users VALUES (?, ?, ?, ?, ?, ?, ?)", (user_id, 0, 0, 0, "", "", ""))
            self.conn.commit()

    def get_custom_command(self, what_you_search, user_id, name=None):
        result = None
        if what_you_search == "names":
            res = self.conn.execute("SELECT Name FROM RPG.Inventory WHERE UserID=?", (user_id,))
            res = res.fetchall()
            for i, value in enumerate(res):
                if str(value[0]).startswith("Custom Command-"):
                    temp_res = str(value[0])
                    temp_res = temp_res[(temp_res.find("-") + 1):]
                    if result is None:
                        result = [temp_res]
                    else:
                        result.append(temp_res)
            return result
        elif what_you_search == "Read":
            res = self.conn.execute("SELECT Type FROM RPG.Inventory WHERE UserID=? AND Name=?", (user_id, name))
            res = res.fetchone()[0]
            result = self.conn.execute("SELECT Read FROM RPG.Custom_Command WHERE ID=?", (res,))
            result = result.fetchone()[0]
            return result
        elif what_you_search == "Write":
            res = self.conn.execute("SELECT Type FROM RPG.Inventory WHERE UserID=? AND Name=?", (user_id, name))
            res = res.fetchone()[0]
            result = self.conn.execute("SELECT Write FROM RPG.Custom_Command WHERE ID=?", (res,))
            result = result.fetchone()[0]
            return result

    async def create_custom_command(self, ctx):
        await self.bot.send_message(ctx.message.channel, "On what message would it activate?")
        msg1 = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
        t1 = str(msg1.content)
        await self.bot.send_message(ctx.message.channel, "What do you want the command to do then?")
        msg2 = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
        t2 = str(msg2.content)
        await self.bot.send_message(ctx.message.channel, "What name do you want to give it?")
        msg3 = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
        t3 = "Custom Command-" + str(msg3.content)
        id_in_custom_command = RPG.add_custom_command(instance, t1, t2)
        RPG.add_item(instance, ctx.message.author.id, t3, 1, id_in_custom_command)
        await self.bot.send_message(ctx.message.channel, "Done!")

    async def rename_custom_command(self, ctx):
        actual = ""
        while True:
            await self.bot.send_message(ctx.message.channel,
                                        "Please send the" + actual + " name of the custom command you "
                                                                     "want to rename")
            msg1 = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
            t1 = str(msg1.content)  # help spacing things a bit, i could have done it in add_item but didn't
            t2 = self.get_custom_command("names", ctx.message.author.id)
            t1 = str(t1)
            if t1 in t2:
                break
            else:
                actual = actual + " ACTUAL"
        await self.bot.send_message(ctx.message.channel, "So, what name do you want to rename it to?")
        msg2 = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
        t2 = str(msg2.content)
        t1 = "Custom Command-" + t1
        t2 = "Custom Command-" + t2
        self.conn.execute("UPDATE RPG.INVENTORY SET Name=? WHERE Name=? AND User_ID=?", (t2, t1, ctx.message.author.id))
        await self.bot.send_message(ctx.message.channel, "Done!")

    async def edit_custom_command(self, ctx):
        actual = ""
        while True:
            await self.bot.send_message(ctx.message.channel,
                                        "Please send the" + actual + " name of the custom command you "
                                                                     "want to edit")
            msg1 = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
            t1 = str(msg1.content)  # help spacing things a bit, i could have done it in add_item but didn't
            t2 = self.get_custom_command("names", ctx.message.author.id)
            t1 = str(t1)
            if t1 in t2:
                break
            else:
                actual = actual + " ACTUAL"
        await self.bot.send_message(ctx.message.channel, "On what message would the edited commmand activate?\n(type 'same' if you don't want to change it)")
        msg2 = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
        t2 = str(msg2.content)
        await self.bot.send_message(ctx.message.channel, "What do you want the command to do then?\n(type 'same' if you don't want to change it)")
        msg3 = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
        t3 = str(msg3.content)
        t1 = "Custom Command-" + t1
        hmm = self.conn.execute("SELECT Type FROM RPG.Inventory WHERE Name=? AND User_ID=?",(t1, ctx.message.author.id))
        hmm = hmm.fetchone()[0]
        if t2.upper() == "SAME" and t3.upper() == "SAME":
            pass
        elif t2.upper() == "SAME":
            self.conn.execute("UPDATE RPG.Custom_Command SET Write=? WHERE ID=?", (t3, hmm))
        elif t3.upper() == "SAME":
            self.conn.execute("UPDATE RPG.Custom_Command SET Read=? WHERE ID=?", (t2, hmm))
        else:
            self.conn.execute("UPDATE RPG.Custom_Command SET Write=?, Read=? WHERE ID=?", (t3, t2, hmm))
        await self.bot.send_message(ctx.message.channel, "Done!")

    async def read_custom_command(self, ctx):
        actual = ""
        while True:
            await self.bot.send_message(ctx.message.channel,
                                        "Please send the" + actual + " name of the custom command you "
                                                                     "want to read")
            msg1 = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
            t1 = str(msg1.content)  # help spacing things a bit, i could have done it in add_item but didn't
            t2 = self.get_custom_command("names", ctx.message.author.id)
            t1 = str(t1)
            if t1 in t2:
                break
            else:
                actual = actual + " ACTUAL"
        read = self.get_custom_command("Read", ctx.message.author.id, t1)
        write = self.get_custom_command("Write", ctx.message.author.id, t1)
        embed = discord.Embed(title=str(t1),
                              description=str("If you send `"+read+"`, then I do `"+write+"`!"),
                              color=0xC0C0C0)
        embed.set_author(name="OwGN-Helper", url="https://discord.gg/QVxwatk",
                         icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
        embed.set_footer(text="Made by BluePhoenixGame, Custom Command module by ClemCa")
        await self.bot.send_message(ctx.message.channel, embed=embed)

    async def delete_custom_command(self, ctx):
        actual = ""
        while True:
            await self.bot.send_message(ctx.message.channel,
                                        "Please send the" + actual + " name of the custom command you "
                                                                     "want to delete")
            msg1 = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
            t1 = str(msg1.content)  # help spacing things a bit, i could have done it in remove_item but didn't
            t2 = self.get_custom_command("names", ctx.message.author.id)
            t1 = str(t1)
            if t1 in t2:
                break
            else:
                actual = actual + " ACTUAL"
        t1 = "Custom Command-" + t1
        hmm = self.conn.execute("SELECT Type FROM RPG.Inventory WHERE Name=? AND User_ID=?", (t1, ctx.message.author.id))
        hmm = hmm.fetchone()[0]
        RPG.remove_custom_command(instance, hmm)
        RPG.remove_item(instance, ctx.message.author.id, t1, 1)
        await self.bot.send_message(ctx.message.channel, "Done!")

    async def execute_read(self, message, check, prefix):
        # The Lorem Ipsum is obviously to stop the replace from replacing the ones preceded by \
        check.replace("\@Prefix", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a dolor in velit luctus maximus.")
        check.replace("@Prefix", prefix)
        check.replace("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a dolor in velit luctus maximus.", "@Prefix")
        # Replace by the prefix (comment serving as spacing)
        check.replace("\@CallUser", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a dolor in velit luctus maximus.")
        check.replace("@CallUser", "{}".format(message.author.mention()))
        check.replace("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a dolor in velit luctus maximus.", "@CallUser")
        # Mention of the user
        check.replace("\@User", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a dolor in velit luctus maximus.")
        check.replace("@User", "{}".format(message.author.name))
        check.replace("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a dolor in velit luctus maximus.", "@User")
        # User's username
        check.replace("\@ReturnToLine", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a dolor in velit luctus maximus.")
        check.replace("@ReturnToLine", "\n")
        check.replace("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a dolor in velit luctus maximus.", "@ReturnToLine")
        # Go back to the line
        return check

    async def execute_write(self, message, value, prefix):
        check = self.get_custom_command("Read", message.author.id, value)
        check = str(check)
        if check.startswith("#PM"):
            check = check[3:]
            destination = message.author
        elif check.startswith("#Channel"):
            check = check[8:]
            destination = message.channel
        else:
            destination = message.channel
        while True:
            if check.startswith("#Wait="):
                check.replace("#Wait=", "", 1)
                temp = 0
                while check.find("#", temp) == check.find("\#"):
                    temp = temp+1
                temp_check = check[:check.find("#", temp)]
                temp_check = float(temp_check)/1000
                await asyncio.sleep(temp_check)
            elif check.startswith("#Edit="):
                check.replace("#Edit=", "", 1)
                temp = 0
                while check.find("#", temp) == check.find("\#"):
                    temp = temp+1
                temp_check = check[:check.find("#", temp)]
                temp_check = await self.execute_read(message, temp_check, prefix)
                self.bot.send_message("{} said: \"".format(message.author.name)+temp_check)
                self.bot.delete_message(message)
            elif check.startswith("#Delete="):
                check.replace("#Delete=", "", 1)
                temp = 0
                while check.find("#", temp) == check.find("\#"):
                    temp = temp+1
                temp_check = check[:check.find("#", temp)]
                temp_check = int(temp_check)

                def is_me(m):
                    return m.author == self.bot.user
                deleted = await self.bot.purge_from(message.channel, limit=temp_check, check=is_me)
                while len(deleted) < temp_check:
                    temp_check = temp_check-deleted
                    deleted = await self.bot.purge_from(message.channel, limit=temp_check, check=is_me)
            elif check.startswith("#Delete"):
                self.bot.delete_message(message)
            elif check.startswith("#Send="):
                check.replace("#Send=", "", 1)
                temp = 0
                while check.find("#", temp) == check.find("\#"):
                    temp = temp+1
                temp_check = check[:check.find("#", temp)]
                temp_check = await self.execute_read(message, temp_check, prefix)
                self.bot.send_message(destination, temp_check)
            elif check.startswith("#End"):
                return
            else:
                self.bot.send_message(destination, "{} There was an error while executing your custom command named ".format(message.author.mention)+prefix+", please don't forget to correct it.")
                return
            temp = 0
            while check.find("#", temp) == check.find("\#"):
                temp = temp+1
            check = check[:check.find("#", temp)]

    @commands.command(pass_context=True)
    async def custom(self, ctx, you_pass=False):
        if you_pass is not False:
            you_pass = str(you_pass).upper()
        else:  # is false
            await self.bot.send_message(ctx.message.channel,
                                        "So, do you want to 'create', 'rename', 'edit', 'read' or 'delete' a custom "
                                        "command? just send either one of them or 'cancel'")
            msg = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
            you_pass = msg.content.upper()
            if you_pass == "CANCEL":
                return
        if you_pass == "CREATE":
            await self.create_custom_command(ctx)
        elif you_pass == "RENAME":
            await self.rename_custom_command(ctx)
        elif you_pass == "EDIT":
            await self.edit_custom_command(ctx)
        elif you_pass == "READ":
            await self.read_custom_command(ctx)
        elif you_pass == "DELETE":
            await self.delete_custom_command(ctx)

    async def on_message(self, message):
        list = self.get_custom_command("names", message.author.id)
        if list is not None:
            for value in list:
                check = self.get_custom_command("Read", message.author.id, value)
                check = str(check)
                prefix = value[15:]
                check = await self.execute_read(message, check, prefix)
                if check.startswith("#PM"):
                    check.replace("#PM", "", 1)
                    if message.channel.is_private:
                        if message.content == check:
                            await self.execute_write(message, value, prefix)
                else:
                    check.replace("#Channel", "", 1)
                    if message.channel.is_private is False:
                        if message.content == check:
                            await self.execute_write(message, value, prefix)

def setup(bot):
    bot.add_cog(CustomCommands(bot))
