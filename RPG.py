import random
import sqlite3
import discord
import asyncio
from discord.ext import commands

conn = sqlite3.connect('users.db', isolation_level=None)
c = conn.cursor()
c.execute("ATTACH DATABASE 'rpg.db' AS RPG")
c.execute("ATTACH DATABASE 'shop.db' AS SHOP")
c.execute("ATTACH DATABASE 'settings.db' AS SETTINGS")
c.execute("""CREATE TABLE IF NOT EXISTS Users(
                      UserID TEXT,
                      Coins FLOAT,
                      SecondCoins FLOAT,
                      Xp INTEGER,
                      Online TEXT,
                      Party TEXT,
                      MyGroup TEXT,
                      Guild TEXT,
                      PRIMARY KEY(UserID))""")
c.execute("""CREATE TABLE IF NOT EXISTS rpg.Inventory(
                      UserID TEXT,
                      Type TEXT,
                      Name TEXT,
                      Quantity INT)""")
c.execute("""CREATE TABLE IF NOT EXISTS SHOP.Shop(
                      UserID TEXT,
                      Item TEXT,
                      Type TEXT,
                      Price FLOAT,
                      Sold TEXT)""")


class RPG:
    def __init__(self, bot):
        self.bot = bot
        global instance
        instance = bot
        global same_rewards_as_others

    def add_custom_command(self, read, write):
        c.execute("INSERT INTO RPG.Custom_Command (Read, Write) VALUES (?, ?)", (read, write))
        ID = c.execute("SELECT MAX(ID) AS LastID FROM RPG.Custom_Command")
        ID = ID.fetchone()[0]
        return ID

    def remove_custom_command(self, ID):
        c.execute("DELETE FROM RPG.Custom_Command WHERE ID=?", (ID,))

    def group_exists(self, user_id):
        res = c.execute("SELECT MyGroup FROM Users WHERE UserID=?", (user_id,))
        res = res.fetchone()[0]
        global same_rewards_as_others
        global group_count
        if res is None or str(res) == "":
            same_rewards_as_others = False
            return False
        else:
            res = c.execute("SELECT MyGroup FROM Users WHERE UserID=?", (user_id,))
            res = res.fetchone()
            if res is not None and res != "":
                res = res[0]
                res = c.execute("SELECT COUNT(*) FROM Users WHERE MyGroup=?", (res,))
                group_count = res.fetchone()[0]
                same_rewards_as_others = True
                res = c.execute("SELECT MyGroup FROM Users WHERE UserID=?", (user_id,))
                group_name = res.fetchone()[0]
                return group_name
            else:
                same_rewards_as_others = False
                return False

    def party_exists(self, user_id):
        res = c.execute("SELECT Party FROM Users WHERE UserID=?", (user_id,))
        res = res.fetchone()[0]
        if str(res) != "":
            res = c.execute("SELECT COUNT(*) FROM Users WHERE MyGroup=?", (res,))
            party_count = res.fetchone()[0]
            if party_count < 1:
                res = c.execute("SELECT Party FROM Users WHERE UserID=?", (user_id,))
                party_name = res.fetchone()[0]
                return party_name
            else:
                return False
        else:
            return False

    def party_members(self, party_name):
        res = c.execute("SELECT COUNT(*) FROM Users WHERE Party=?", (party_name,))
        party_count = res.fetchone()[0]
        return party_count

    def guild_exists(self, user_id):
        res = c.execute("SELECT Guild FROM Users WHERE UserID=?", (user_id,))
        res = res.fetchone()[0]
        if str(res) != "":
            res = c.execute("SELECT COUNT(*) FROM Users WHERE Guild=?", (res,))
            guild_count = res.fetchone()[0]
            if guild_count < 1:
                res = c.execute("SELECT Guild FROM Users WHERE UserID=?", (user_id,))
                guild_name = res.fetchone()[0]
                return guild_name
            else:
                return False
        else:
            return False

    def get_item(self, user_id, name, the_type=None):
        from currency import Currency
        Currency.create_user_if_not_exists(instance, user_id)
        if the_type is None:
            res = c.execute("SELECT COUNT(*) FROM RPG.Inventory WHERE UserID=? AND Name=?",
                                    (str(user_id), name))
            res = res.fetchone()[0]
            if res >= 1:
                res = c.execute("SELECT Quantity FROM RPG.Inventory WHERE UserID=? AND Name=?",
                                        (str(user_id), name))
                if res is not None:
                    item = int(res.fetchone()[0])
                    return item
                else:
                    return None
            else:
                return None
        else:
            res = c.execute("SELECT COUNT(*) FROM RPG.Inventory WHERE UserID=? AND Name=? AND Type=?",
                                    (str(user_id), name, the_type))
            res = res.fetchone()[0]
            if res >= 1:
                res = c.execute("SELECT Quantity FROM RPG.Inventory WHERE UserID=? AND Name=? AND Type=?",
                                        (str(user_id), name, the_type))
                if res is not None:
                    item = int(res.fetchone()[0])
                    return item
                else:
                    return None
            else:
                return None

    def get_type(self, name):
        res = c.execute("SELECT Type FROM SETTINGS.Reference WHERE Name=?", (name,))
        return res.fetchone()[0]

    def add_group_coin(self, group_name, coins):
        from currency import Currency
        print("I took the dangerous path")
        res = c.execute("SELECT UserID FROM Users WHERE MyGroup=?", (group_name,))
        user_ids = res.fetchall()
        for x, value in enumerate(user_ids):
            temp_user_id_to_correct_everything = str(user_ids[x])
            temp_user_id_to_correct_everything = temp_user_id_to_correct_everything[2:-3]
            print("ID n°" + str(x) + ": " + temp_user_id_to_correct_everything)
            Currency.add_coin(instance, temp_user_id_to_correct_everything, coins)

    def add_item(self, user_id, name, amount, the_type=None, description=None):
        user_id = str(user_id)
        if user_id.startswith("('"):
            user_id = user_id[2:-3]
        the_type = str(the_type)
        if the_type.startswith("('"):
            the_type = the_type[2:-3]
        quantity = RPG.get_item(instance, user_id, name)
        if the_type is None:
            if quantity is None:
                c.execute("INSERT INTO RPG.Inventory VALUES (?, ?, ?, ?, ?)",
                                  (user_id, RPG.get_type(instance, name), name, amount, description))
            else:
                quantity = quantity + amount
                c.execute("UPDATE RPG.Inventory SET Quantity=? WHERE UserID=? AND Name=?",
                                  (quantity, user_id, name))
        else:
            if quantity is None:
                c.execute("INSERT INTO RPG.Inventory VALUES (?, ?, ?, ?, ?)",
                                  (user_id, the_type, name, amount, description))
            else:
                quantity = quantity + amount
                c.execute("UPDATE RPG.Inventory SET Quantity=? WHERE UserID=? AND Name=?",
                                  (quantity, user_id, name))


    def remove_item(self, user_id, name, amount, the_type=None):
        quantity = RPG.get_item(instance, user_id, name, the_type)
        if quantity is None:
            return
        if the_type is None:
            if quantity <= amount:
                c.execute("DELETE FROM RPG.Inventory WHERE UserID=? AND Name=?", (user_id, name))
            else:
                quantity = quantity - amount
                c.execute("UPDATE RPG.Inventory SET Quantity=? WHERE UserID=? AND Name=?",
                                  (quantity, user_id, name))
        else:
            if quantity <= amount:
                c.execute("DELETE FROM RPG.Inventory WHERE UserID=? AND Name=? AND Type=?",
                                  (user_id, name, the_type))
            else:
                quantity = quantity - amount
                c.execute("UPDATE RPG.Inventory SET Quantity=? WHERE UserID=? AND Name=? AND Type=?",
                                  (quantity, user_id, name, the_type))

    def add_group_item(self, party_name, name, amount, the_type=None):
        res = c.execute("SELECT UserID FROM Users WHERE MyGroup=?", (party_name,))
        user_ids = res.fetchall()
        if the_type is None:
            for x, value in enumerate(user_ids):
                RPG.add_item(instance, user_ids[x], name, amount)
        else:
            for x, value in enumerate(user_ids):
                RPG.add_item(instance, user_ids[x], name, amount, the_type)

    def remove_group_item(self, party_name, name, amount, the_type=None):
        res = c.execute("SELECT UserID FROM Users WHERE MyGroup=?", (party_name,))
        user_ids = res.fetchall()
        if the_type is None:
            for x, value in enumerate(user_ids):
                RPG.remove_item(instance, user_ids[x], name, amount)
        else:
            for x, value in enumerate(user_ids):
                RPG.remove_item(instance, user_ids[x], name, amount, the_type)

    @commands.cooldown(1, 15*3600, commands.BucketType.user)
    @commands.command(pass_context=True)
    async def Cancel(self, ctx):
        res = c.execute("SELECT COUNT(*) FROM RPG.Inventory WHERE UserID=? AND Name='Quest Cancel'",
                                (ctx.message.author.id,))
        res = res.fetchone()[0]
        if res < 1:
            self.bot.send_message(ctx.message.channel,
                                  "You do not own this command, check the shop if you want to buy it, or find a player willing to exchange it with you")
            return
        else:
            res = c.execute(
                "SELECT COUNT(*) FROM RPG.Inventory WHERE UserID=? AND Name='Quest Cancel' AND Type='permanent command token'",
                (ctx.message.author.id,))
            res = res.fetchone()[0]
            if res < 1:
                RPG.remove_item(instance, ctx.message.author.id, "Quest Cancel", 1, "one use command")
            c.execute("UPDATE SETTINGS.Quest SET Activated=Activated+1")

    @commands.cooldown(1, 1*3600*24*7, commands.BucketType.user)
    @commands.command(pass_context=True)
    async def Steal(self, ctx, member: discord.Member = None, currency=None):
        res = c.execute("SELECT COUNT(*) FROM RPG.Inventory WHERE UserID=? AND Name='Steal'",
                                (ctx.message.author.id,))
        res = res.fetchone()[0]
        if res < 1:
            self.bot.send_message(ctx.message.channel, "You do not own this command, check the shop if you want to buy it, or find a player willing to exchange it with you")
            return
        else:
            if member is None:
                self.bot.send_message(ctx.message.channel, "Do you want to steal None's money? I'm sure he'll be happy to give you nothing... A mention! A! Men! Tion!")
            else:
                if currency is None:
                    self.bot.send_message(ctx.message.channel, "Gold or Silver? You didn't mention this...")
                else:
                    if currency.upper() == "GOLD":
                        currency = 1
                    elif currency.upper() == "SILVER":
                        currency = 2
                    else:
                        self.bot.send_message(ctx.message.channel, "Please send a valid currency name")
                        return
                    res = c.execute(
                        "SELECT COUNT(*) FROM RPG.Inventory WHERE UserID=? AND Name='Steal' AND Type='permanent command token'",
                        (ctx.message.author.id,))
                    res = res.fetchone()[0]
                    if res < 1:
                        RPG.remove_item(instance, ctx.message.author.id, "Steal", 1, "one use command")
                    from currency import Currency
                    target = Currency.get_coin(instance, member, currency)
                    if target < 10000:
                        objective = target
                    else:
                        objective = 10000
                    so = random.randint(0, objective)
                    Currency.remove_coin(instance, member, so, currency)
                    Currency.add_coin(instance, ctx.message.author.id, so, currency)
                    self.bot.send_message(ctx.message.channel, "You stole "+str(so)+" from "+str(member.name)+"!")

    @commands.command(pass_context=True)
    async def Equivalent(self, ctx, member: discord.Member = None):
        res = c.execute("SELECT COUNT(*) FROM RPG.Inventory WHERE UserID=? AND Name='Equivalent Exchange'",
                        (ctx.message.author.id,))
        res = res.fetchone()[0]
        if res < 1:
            self.bot.send_message(ctx.message.channel, "You do not own this command, check the shop if you want to buy it")
            return
        else:
            res = c.execute(
                "SELECT COUNT(*) FROM RPG.Inventory WHERE UserID=? AND Name='Equivalent Exchange' AND Type='permanent command token'",
                (ctx.message.author.id,))
            res = res.fetchone()[0]
            if res < 1:
                RPG.remove_item(instance, ctx.message.author.id, "Equivalent Exchange", 1, "one use command")
            # then
            if member is None:
                self.bot.send_message(ctx.message.channel, "Please specify the user you want to propose the trade to")
                return
            self.bot.send_message(member, "{} asked you for an Equivalent Exchange trade, do you want to trade? Send any message to accept or wait 60 seconds if you don't want to.".format(ctx.message.author.name))
            self.bot.send_message(ctx.message.author, "You demand was sent, if it is not accepted within a minute, the trade will be cancelled.")
            msg = await self.bot.wait_for_message(timeout=60, author=ctx.message.author, channel=ctx.message.channel)
            if msg is None:
                self.bot.send_message(member, "Trade timeout")
                self.bot.send_message(ctx.message.author, "Trade timeout")
                return
            self.bot.send_message(ctx.message.author, "Trade accepted")
            from shop import Shop
            await Shop.inventory_page(instance, ctx.message.author.id, "max", "Your items", True, True, ctx.message.author)
            d = await Shop.inventory_page(instance, ctx.message.author.id, "max", "All the items in the inventory of {}, send the corresponding number to select. You have five minutes".format(ctx.message.author.name), True, True, member)
            await Shop.inventory_page(instance, member.id, "max", "Your items", True, True, member)
            dd = await Shop.inventory_page(instance, member.id, "max", "All the items in the inventory of {}, send the corresponding number to select. You have five minutes".format(member.name), True, True, ctx.message.author)
            h = await self.bot.start_private_message(member)
            hh = await self.bot.start_private_message(ctx.message.author)
            selected_1 = []
            selected_2 = []
            total_value_1 = 0
            total_value_2 = 0
            turn = False
            while True:
                if turn is False:
                    cc = await self.bot.wait_for_message(timeout=300, author=member, channel=h)
                    self.bot.send_message(hh, "{} is currently selecting an item, you turn comes next".format(member.name))
                    if cc is None:
                        self.bot.send_message(member, "Trade timeout")
                        self.bot.send_message(ctx.message.author, "Trade timeout")
                        return
                    else:
                        if cc.content.isdigit():
                            temp = c.execute("SELECT Value FROM SETTINGS.Reference WHERE Name=?", (d[cc],))
                            temp = temp.fetchone()[0]
                            temp = int(temp)
                            cc = int(cc.content)
                            if cc in selected_1:
                                selected_1.remove(cc)
                                total_value_1 = total_value_1 - temp
                            else:
                                selected_1.append(cc)
                                total_value_1 = total_value_1 + temp
                        else:
                            if cc.content.upper == "END" or cc.content.upper == "CONFIRM":
                                self.bot.send_message(member, "Trade concluded")
                                self.bot.send_message(ctx.message.author, "Trade concluded by {}".format(member.name))
                                break
                            self.bot.send_message(member, "Trade cancel")
                            self.bot.send_message(ctx.message.author, "Trade cancel")
                            return
                    turn = True
                else:
                    cc = await self.bot.wait_for_message(timeout=300, author=member, channel=h)
                    self.bot.send_message(h, "{} is currently selecting an item, you turn comes next".format(ctx.message.author.name))
                    if cc is None:
                        self.bot.send_message(member, "Trade timeout")
                        self.bot.send_message(ctx.message.author, "Trade timeout")
                        return
                    else:
                        if cc.content.isdigit():
                            temp = c.execute("SELECT Value FROM SETTINGS.Reference WHERE Name=?", (dd[cc],))
                            temp = temp.fetchone()[0]
                            cc = int(cc.content)
                            if cc in selected_2:
                                selected_2.remove(cc)
                                total_value_2 = total_value_2 - temp
                            else:
                                selected_2.append(cc)
                                total_value_2 = total_value_2 + temp
                        else:
                            if cc.content.upper == "END" or cc.content.upper == "CONFIRM":
                                self.bot.send_message(ctx.message.author, "Trade concluded")
                                self.bot.send_message(member, "Trade concluded by {}".format(ctx.message.author.name))
                                break
                            self.bot.send_message(member, "Trade cancel")
                            self.bot.send_message(ctx.message.author, "Trade cancel")
                            return
                    turn = False
                self.bot.send_message(member, "Your selected items: "+str(selected_1)+" for a total value of "+total_value_1+"\n{}'s selected items: ".format(member.name)+str(selected_2)+" for a total value of "+total_value_2)
                self.bot.send_message(ctx.message.author, "Your selected items: "+str(selected_2)+" for a total value of "+total_value_2+"\n{}'s selected items: ".format(member.name)+str(selected_1)+" for a total value of "+total_value_1)
            for i, value in enumerate(selected_1):
                self.remove_item(ctx.message.author.id, selected_1[i], 1)
                self.add_item(member.id, selected_1[i], 1)
            for i, value in enumerate(selected_2):
                self.remove_item(member.id, selected_2[i], 1)
                self.add_item(ctx.message.author.id, selected_2[i], 1)



    async def on_message(self, message, refund=None):
        if message.author.id == self.bot.user.id:
            return
        else:
            from currency import Currency
            group = RPG.group_exists(instance, message.author.id)
            # as it wanted to consider self as bot instead of RPG
            if group is False:
                same_rewards_as_others = False
            else:
                same_rewards_as_others = True
            if message.content == "Please v155428" or message.content == "Press F to pay respects" or message.content == "UnrivaledSuperHottie":
                chancesForQuest = 1
            else:
                chancesForQuest = 0.01
            should_get_quest = (chancesForQuest >= (random.random())) is True
            chancesForQuest = 0.01
            if (should_get_quest is True) or (refund == "refund18325478"):
                guild_name = RPG.guild_exists(instance, message.author.id)
                party_name = RPG.party_exists(instance, message.author.id)
                group_name = RPG.group_exists(instance, message.author.id)
                if party_name is False:
                    how_much_members = False
                    bonus = 0
                    malus = 0
                else:
                    how_much_members = int(RPG.party_members(instance, party_name))
                if how_much_members == 2:
                    bonus = 5
                    malus = 5
                elif how_much_members == 3:
                    bonus = 10
                    malus = 10
                elif how_much_members == 4:
                    bonus = 15
                    malus = 15
                elif how_much_members == 5:  # up to 50% bonus
                    bonus = 20
                    malus = 20
                res = c.execute("SELECT Id FROM SETTINGS.Rewards")
                res = res.fetchall()
                temporary = res
                res = []
                for bruh, value in enumerate(temporary):
                    res.extend(temporary[bruh])
                giga = []
                for i, value in enumerate(res):
                    rest = c.execute("SELECT Main_Tag FROM SETTINGS.Rewards")
                    rest = rest.fetchall()
                    temporary = rest
                    rest = []
                    for bruh, bruhlue in enumerate(temporary):
                        rest.extend(temporary[bruh])
                    rest = rest[i]
                    if rest not in giga:
                        giga.append(rest)
                for i, value in enumerate(res):
                    rest = c.execute("SELECT Tag2 FROM SETTINGS.Rewards")
                    rest = rest.fetchall()
                    temporary = rest
                    rest = []
                    for bruh, bruhlue in enumerate(temporary):
                        rest.extend(temporary[bruh])
                    rest = rest[i]
                    if rest not in giga:
                        giga.append(rest)
                for i, value in enumerate(res):
                    rest = c.execute("SELECT Tag3 FROM SETTINGS.Rewards")
                    rest = rest.fetchall()
                    temporary = rest
                    rest = []
                    for bruh, bruhlue in enumerate(temporary):
                        rest.extend(temporary[bruh])
                    rest = rest[i]
                    if rest not in giga:
                        giga.append(rest)
                theme = random.choice(giga)
                while theme == 'All':
                    theme = random.choice(giga)
                res = c.execute(
                    "SELECT Id FROM SETTINGS.Rewards WHERE Main_Tag=? OR Tag2=? OR Tag3=? OR Main_Tag='All'",
                    (theme, theme, theme))
                res = res.fetchall()
                temporary = res
                res = []
                for bruh, value in enumerate(temporary):
                    res.extend(temporary[bruh])
                e = list(res)
                first = random.choice(e)
                second = random.choice(e)
                while second == first:
                    second = random.choice(e)
                third = random.choice(e)
                while third == first or third == second:
                    third = random.choice(e)
                fourth = random.choice(e)
                while fourth == first or fourth == second or fourth == third:
                    fourth = random.choice(e)
                fifth = random.choice(e)
                while fifth == first or fifth == second or fifth == third or fifth == fourth:
                    fifth = random.choice(e)
                the_one = first
                count = 1
                chances_1 = 0
                chances_2 = 0
                chances_3 = 0
                chances_4 = 0
                chances_5 = 0
                while True:
                    if count == 2:
                        the_one = second
                    if count == 3:
                        the_one = third
                    if count == 4:
                        the_one = fourth
                    if count == 5:
                        the_one = fifth
                    if count == 6:
                        break
                    res = c.execute("SELECT Rarity_level FROM SETTINGS.Rewards WHERE Id=?", (the_one,))
                    res = res.fetchone()[0]
                    if count == 1:
                        chances_1 = res
                    if count == 2:
                        chances_2 = res
                    if count == 3:
                        chances_3 = res
                    if count == 4:
                        chances_4 = res
                    if count == 5:
                        chances_5 = res
                    count = count + 1
                while True:
                    if chances_1 < chances_2 < chances_3 < chances_4 < chances_5:
                        break
                    if chances_1 > chances_2:
                        temp1 = chances_2
                        temp2 = second
                        chances_2 = chances_1
                        second = first
                        chances_1 = temp1
                        first = temp2
                    if chances_2 > chances_3:
                        temp1 = chances_3
                        temp2 = third
                        chances_3 = chances_2
                        third = second
                        chances_2 = temp1
                        second = temp2
                    if chances_3 > chances_4:
                        temp1 = chances_4
                        temp2 = fourth
                        chances_4 = chances_3
                        fourth = third
                        chances_3 = temp1
                        third = temp2
                    if chances_4 > chances_5:
                        temp1 = chances_5
                        temp2 = fifth
                        chances_5 = chances_4
                        fifth = fourth
                        chances_4 = temp1
                        fourth = temp2
                total = chances_1 + chances_2 + chances_3 + chances_4 + chances_5
                chances_1 = total - chances_1
                chances_2 = total - chances_2
                chances_3 = total - chances_3
                chances_4 = total - chances_4
                chances_5 = total - chances_5
                total = chances_1 + chances_2 + chances_3 + chances_4 + chances_5
                chances_1 = (chances_1 * 100 / total)
                chances_2 = (chances_2 * 100 / total)
                chances_3 = (chances_3 * 100 / total)
                chances_4 = (chances_4 * 100 / total)
                chances_5 = (chances_5 * 100 / total)
                if malus <= chances_1:
                    chances_1 = chances_1 - malus
                    bonus = bonus / 4
                    chances_2 = chances_2 + bonus
                    chances_3 = chances_3 + bonus
                    chances_4 = chances_4 + bonus
                    chances_5 = chances_5 + bonus
                elif malus <= (chances_1 + chances_2):
                    chances_1 = 0
                    malus = malus - chances_1
                    chances_2 = chances_2 - malus
                    bonus = bonus / 3
                    chances_3 = chances_3 + bonus
                    chances_4 = chances_4 + bonus
                    chances_5 = chances_5 + bonus
                elif malus <= (chances_1 + chances_2 + chances_3):
                    chances_1 = 0
                    chances_2 = 0
                    malus = malus - chances_1 - chances_2
                    chances_3 = chances_3 - malus
                    bonus = bonus / 2
                    chances_4 = chances_4 + bonus
                    chances_5 = chances_5 + bonus
                elif malus <= (chances_1 + chances_2 + chances_3):
                    chances_1 = 0
                    chances_2 = 0
                    chances_3 = 0
                    malus = malus - chances_1 - chances_2 - chances_3
                    chances_4 = chances_4 - malus
                    chances_5 = chances_5 + bonus
                else:
                    chances_1 = 0
                    chances_2 = 0
                    chances_3 = 0
                    chances_4 = 0
                    chances_5 = 100
                if message.content == "UnrivaledSuperHottie":
                    chances_1 = 0
                    chances_2 = 0
                    chances_3 = 0
                    chances_4 = 0
                    chances_5 = 100
                the_one = first
                count = 1
                id1 = 0
                id2 = 0
                id3 = 0
                id4 = 0
                id5 = 0
                while True:
                    if count == 2:
                        the_one = second
                    if count == 3:
                        the_one = third
                    if count == 4:
                        the_one = fourth
                    if count == 5:
                        the_one = fifth
                    if count == 6:
                        break
                    res = c.execute("SELECT Name FROM SETTINGS.Rewards WHERE Id=?", (the_one,))
                    res = res.fetchone()[0]
                    if count == 1:
                        id1 = first
                        first = res
                    if count == 2:
                        id2 = second
                        second = res
                    if count == 3:
                        id3 = third
                        third = res
                    if count == 4:
                        id4 = fourth
                        fourth = res
                    if count == 5:
                        id5 = fifth
                        fifth = res
                    count = count + 1
                res = c.execute(
                    "SELECT COUNT(*) FROM SETTINGS.PlotStart WHERE MainTheme=? OR Theme2=? OR Theme3=? OR MainTheme='All'",
                    (theme, theme, theme))
                res = res.fetchone()[0]
                res = random.randint(0, (res - 1))
                cool_guy = res
                saved = c.execute("SELECT SpecialRelation FROM SETTINGS.PlotStart WHERE Id=?", (cool_guy,))
                if saved is None:
                    pass
                else:
                    saved = saved.fetchone()[0]
                res = c.execute("SELECT Subject FROM SETTINGS.PlotStart WHERE Id=?", (cool_guy,))
                res = res.fetchone()[0]
                subject = str(res)
                res = c.execute("SELECT UniqueQuest FROM SETTINGS.PlotStart WHERE Id=?", (cool_guy,))
                res = res.fetchone()[0]
                if res is None:
                    unique = res
                else:
                    unique = bool(res)
                res = c.execute("SELECT Content FROM SETTINGS.PlotStart WHERE Id=?", (cool_guy,))
                res = res.fetchone()[0]
                res = str(res)
                part1 = res[:res.find("{")]
                part3 = res[(res.find("}") + 1):]
                if unique is not None:
                    if saved is not None:
                        res = c.execute(
                            "SELECT COUNT(*) FROM SETTINGS.PlotAction WHERE MainTheme=? OR Theme2=? OR Theme3=? OR SpecialRelation=?",
                            (theme, theme, theme, saved))
                    else:
                        res = c.execute(
                            "SELECT COUNT(*) FROM SETTINGS.PlotAction WHERE MainTheme=? OR Theme2=? OR Theme3=?",
                            (theme, theme, theme))
                else:
                    if saved is not None:
                        res = c.execute(
                            "SELECT COUNT(*) FROM SETTINGS.PlotAction WHERE MainTheme=? OR Theme2=? OR Theme3=? OR MainTheme='All' OR SpecialRelation=?",
                            (theme, theme, theme, saved))
                    else:
                        res = c.execute(
                            "SELECT COUNT(*) FROM SETTINGS.PlotAction WHERE MainTheme=? OR Theme2=? OR Theme3=? OR MainTheme='All'",
                            (theme, theme, theme))
                res = res.fetchone()[0]
                res = random.randint(0, (res - 1))
                if unique is not None:
                    if saved is not None:
                        res = c.execute("SELECT Content FROM SETTINGS.PlotAction WHERE Id=? AND (MainTheme=? OR Theme2=? OR Theme3=? OR SpecialRelation=?)", (res, theme, theme, theme, saved))
                        res = res.fetchone()[0]
                    else:
                        res = c.execute("SELECT Content FROM SETTINGS.PlotAction WHERE Id=? AND (MainTheme=? OR Theme2=? OR Theme3=?)", (res, theme, theme, theme))
                        res = res.fetchone()[0]
                else:
                    if saved is not None:
                        res = c.execute("SELECT Content FROM SETTINGS.PlotAction WHERE Id=? AND (MainTheme=? OR Theme2=? OR Theme3=? OR MainTheme='All' OR SpecialRelation=?)", (res, theme, theme, theme, saved))
                        res = res.fetchone()[0]
                    else:
                        res = c.execute("SELECT Content FROM SETTINGS.PlotAction WHERE Id=? AND (MainTheme=? OR Theme2=? OR Theme3=? OR MainTheme='All')", (res, theme, theme, theme))
                        res = res.fetchone()[0]
                res = str(res)
                part2 = res
                quest = part1 + part2 + part3
                embed = discord.Embed(title="You got a quest",
                                      description=quest + "\n\nDo you want to accept the quest?\nIf yes, send \"Yes\" within 30 seconds.\nElse, send \"No\" or wait for 30 seconds",
                                      color=0x0000ff)
                embed.set_author(name="OwGN-Helper", url="https://discord.gg/QVxwatk",
                                 icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
                if how_much_members is not False:
                    embed.add_field(name="Party members", value=str(how_much_members))
                else:
                    embed.add_field(name="Party", value="No party member, need some? Use &Rhelp")
                embed.add_field(name="Cost:", value="15", inline=False)
                embed.add_field(name="Chances of obtaining " + str(first),
                                value="About " + str(round(chances_1, 2)) + "%",
                                inline=True)
                embed.add_field(name="Chances of obtaining " + str(second),
                                value="About " + str(round(chances_2, 2)) + "%",
                                inline=True)
                embed.add_field(name="Chances of obtaining " + str(third),
                                value="About " + str(round(chances_3, 2)) + "%", inline=True)
                embed.add_field(name="Chances of obtaining " + str(fourth),
                                value="About " + str(round(chances_4, 2)) + "%", inline=True)
                embed.add_field(name="Chances of obtaining " + str(fifth),
                                value="About " + str(round(chances_5, 2)) + "%",
                                inline=True)
                embed.set_footer(text="Made by BluePhoenixGame, RPG module by ClemCa")
                he_he = await self.bot.send_message(message.channel, embed=embed)
                msg = await self.bot.wait_for_message(timeout=30, author=message.author, channel=message.channel)
                self.bot.delete_message(he_he)
                if msg is None:
                    he_he = await self.bot.send_message(
                        "Time's up")  # depending of the character who gave the quest, example: the old woman walked away
                    await asyncio.sleep(2)
                    self.bot.delete_message(he_he)
                if msg.content.upper() == 'NO':
                    self.bot.delete_message(msg)
                    he_he = await self.bot.send_message(message.channel, "Quest cancelled")
                    await asyncio.sleep(2)
                    self.bot.delete_message(he_he)
                if msg.content.upper() == 'YES':
                    self.bot.delete_message(msg)
                    lol = await self.bot.send_message(message.channel,
                                                      "What currency do you want to use? Gold or Silver?")
                    msg = await self.bot.wait_for_message(timeout=30, author=message.author, channel=message.channel)
                    self.bot.delete_message(lol)
                    if msg is None:
                        he_he = await self.bot.send_message(
                            "Time's up")  # depending of the character who gave the quest, example: the old woman walked away
                        await asyncio.sleep(2)
                        self.bot.delete_message(he_he)
                        pass
                    if msg.content.upper() == 'GOLD':
                        money = Currency.get_coin(instance, message.author.id, 1)
                        self.bot.delete_message(msg)
                        pass
                    elif msg.content.upper() == 'SILVER':
                        money = Currency.get_coin(instance, message.author.id, 2)
                        self.bot.delete_message(msg)
                    else:
                        he_he = await self.bot.send_message(
                            msg.channel,
                            "Incorrect answer")  # depending too, example: "are you sure you're fine, young man?"
                        await asyncio.sleep(2)
                        self.bot.delete_message(he_he)
                        return
                    if money > 15:
                        # do the quest roll
                        result = random.randint(1, 100)
                        if result <= chances_1:
                            result = str(first)
                            rewardnumber = 1
                        elif result <= chances_1 + chances_2:
                            result = str(second)
                            rewardnumber = 2
                        elif result <= chances_1 + chances_2 + chances_3:
                            result = str(third)
                            rewardnumber = 3
                        elif result <= chances_1 + chances_2 + chances_3 + chances_4:
                            result = str(fourth)
                            rewardnumber = 4
                        elif result <= chances_1 + chances_2 + chances_3 + chances_4 + chances_5:
                            result = str(fifth)
                            rewardnumber = 5
                        else:
                            result = "Hmm, seems like there's a problem, please report the bug to BluePhoenixGame or ClemCa, we'll refund your money and give you another quest"
                            refund = True
                            rewardnumber = 1
                        if refund is True:
                            embed = discord.Embed(title="Error", description=result, color=0xff0000)
                            embed.set_author(name="OwGN-Helper", url="https://discord.gg/QVxwatk",
                                             icon_url="https://vignette.wikia.nocookie.net/v__/images/8/8d/Gohon%27nin_ga_Iyagatteru_no_ni_Moushi_Wake_Arimasen.png/revision/latest?cb=20150522181625&path-prefix=vocaloidlyrics")
                            embed.set_footer(text="Made by BluePhoenixGame, RPG module by ClemCa")
                            await self.bot.send_message(message.channel, embed=embed)
                        else:
                            verification = c.execute("SELECT Activated FROM SETTINGS.Quest")
                            verification = verification.fetchone()[0]
                            if verification == 0:
                                to_use = c.execute("SELECT Result FROM SETTINGS.ResultPerSubject WHERE Subject=?", (subject,))
                                to_use = to_use.fetchone()[0]
                                to_use = str(to_use)
                                if "{}" in to_use:
                                    to_use_part_1 = to_use[:to_use.find("{")]
                                    to_use_part_2 = to_use[(to_use.find("}")+1):]
                                else:
                                    to_use_part_1 = None
                                    to_use_part_2 = None
                                if to_use_part_1 is None:
                                    embed = discord.Embed(title="Quest Result",
                                                          description=to_use,
                                                          color=0x0000ff)
                                    embed.set_author(name="OwGN-Helper", url="https://discord.gg/QVxwatk",
                                                     icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
                                    embed.set_footer(text="Made by BluePhoenixGame, RPG module by ClemCa")
                                else:
                                    embed = discord.Embed(title="Quest Result",
                                                          description=to_use_part_1+result+to_use_part_2,
                                                          color=0x0000ff)
                                    embed.set_author(name="OwGN-Helper", url="https://discord.gg/QVxwatk",
                                                     icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
                                    embed.set_footer(text="Made by BluePhoenixGame, RPG module by ClemCa")
                            else:
                                c.execute("UPDATE SETTINGS.Quest SET Activated=Activated-1")
                                to_use = c.execute("SELECT ResultCancelled FROM SETTINGS.ResultPerSubject WHERE Subject=?", (subject,))
                                to_use = to_use.fetchone()[0]
                                to_use = str(to_use)
                                if "{}" in to_use:
                                    to_use_part_1 = to_use[:to_use.find("{")]
                                    to_use_part_2 = to_use[(to_use.find("}")+1):]
                                else:
                                    to_use_part_1 = None
                                    to_use_part_2 = None
                                if to_use_part_1 is None:
                                    embed = discord.Embed(title="Quest Result",
                                                          description=to_use,
                                                          color=0x0000ff)
                                    embed.set_author(name="OwGN-Helper", url="https://discord.gg/QVxwatk",
                                                     icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
                                    embed.set_footer(text="Made by BluePhoenixGame, RPG module by ClemCa")
                                else:
                                    embed = discord.Embed(title="Quest Result",
                                                          description=to_use_part_1+result+to_use_part_2,
                                                          color=0x0000ff)
                                    embed.set_author(name="OwGN-Helper", url="https://discord.gg/QVxwatk",
                                                     icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
                                    embed.set_footer(text="Made by BluePhoenixGame, RPG module by ClemCa")
                                rewardnumber = 1
                                first = "Nothing"
                            await self.bot.send_message(message.channel, embed=embed)
                        RPG.group_exists(instance, message.author.id)
                        if refund is True:
                            Currency.add_coin(instance, message.author.id, 15)
                            empty_message = message
                            empty_message.content = ""
                            refund = "refund18325478"
                            await RPG.on_message(instance, empty_message, refund)
                        else:
                            type1 = ""
                            type2 = ""
                            type3 = ""
                            type4 = ""
                            type5 = ""
                            count = 1
                            while True:
                                # YOU CAN'T FUCKING EDIT VARIABLES IN A WHILE LOOP........ It reverts when it ends
                                if count == 1:
                                    res = c.execute("SELECT Type FROM SETTINGS.Rewards WHERE Id=?", (id1,))
                                    lol = res.fetchone()[0]
                                    type1 = lol
                                elif count == 2:
                                    res = c.execute("SELECT Type FROM SETTINGS.Rewards WHERE Id=?", (id2,))
                                    lol = res.fetchone()[0]
                                    type2 = lol
                                elif count == 3:
                                    res = c.execute("SELECT Type FROM SETTINGS.Rewards WHERE Id=?", (id3,))
                                    lol = res.fetchone()[0]
                                    type3 = lol
                                elif count == 4:
                                    res = c.execute("SELECT Type FROM SETTINGS.Rewards WHERE Id=?", (id4,))
                                    lol = res.fetchone()[0]
                                    type4 = lol
                                elif count == 5:
                                    res = c.execute("SELECT Type FROM SETTINGS.Rewards WHERE Id=?", (id5,))
                                    lol = res.fetchone()[0]
                                    type5 = lol
                                else:
                                    break
                                count = count + 1
                            reward = ""
                            reward_type = ""
                            if same_rewards_as_others is True:
                                if rewardnumber == 1:
                                    if first == "Nothing":
                                        pass
                                    else:
                                        reward = first
                                        reward_type = type1
                                elif rewardnumber == 2:
                                    if second == "Nothing":
                                        pass
                                    else:
                                        reward = second
                                        reward_type = type2
                                elif rewardnumber == 3:
                                    if third == "Nothing":
                                        pass
                                    else:
                                        reward = third
                                        reward_type = type3
                                elif rewardnumber == 4:
                                    if fourth == "Nothing":
                                        pass
                                    else:
                                        reward = fourth
                                        reward_type = type4
                                elif rewardnumber == 5:
                                    if fifth == "Nothing":
                                        pass
                                    else:
                                        reward = fifth
                                        reward_type = type5
                                if reward != "":
                                    if " " in str(reward):
                                        reward = str(reward)[:str(reward).find(" ")]
                                    else:
                                        RPG.add_group_item(instance, group_name, str(reward), 1, reward_type)
                            else:
                                if rewardnumber == 1:
                                    if first == "Nothing":
                                        pass
                                    else:
                                        reward = first
                                        reward_type = type1
                                elif rewardnumber == 2:
                                    if second == "Nothing":
                                        pass
                                    else:
                                        reward = second
                                        reward_type = type2
                                elif rewardnumber == 3:
                                    if third == "Nothing":
                                        pass
                                    else:
                                        reward = third
                                        reward_type = type3
                                elif rewardnumber == 4:
                                    if fourth == "Nothing":
                                        pass
                                    else:
                                        reward = fourth
                                        reward_type = type4
                                elif rewardnumber == 5:
                                    if fifth == "Nothing":
                                        pass
                                    else:
                                        reward = fifth
                                        reward_type = type5
                                if reward != "":
                                    if " " in str(reward):
                                        reward = str(reward)[:str(reward).find(" ")]
                                        RPG.add_item(instance, message.author.id, str(reward), 1, reward_type)
                    else:
                        await self.bot.send_message(message.channel, "You do not have enough money")


def setup(bot):
    bot.add_cog(RPG(bot))
