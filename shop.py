import sqlite3

import discord
from discord.ext import commands

conn = sqlite3.connect('users.db', isolation_level=None)
c = conn.cursor()
c.execute("ATTACH DATABASE 'rpg.db' AS RPG")
c.execute("ATTACH DATABASE 'shop.db' AS SHOP")
c.execute("ATTACH DATABASE 'settings.db' AS SETTINGS")
c.execute("""CREATE TABLE IF NOT EXISTS Users(
              UserID TEXT,
              Coins FLOAT,
              SecondCoins FLOAT,
              Xp INTEGER,
              Online TEXT,
              Party TEXT,
              MyGroup TEXT,
              Guild TEXT,
              PRIMARY KEY(UserID))""")
c.execute("""CREATE TABLE IF NOT EXISTS rpg.Inventory(
              UserID TEXT,
              Type TEXT,
              Name TEXT,
              Quantity INT)""")
c.execute("""CREATE TABLE IF NOT EXISTS SHOP.Shop(
              UserID TEXT,
              Item TEXT,
              Type TEXT,
              Price FLOAT,
              Sold TEXT)""")


class Shop:
    def __init__(self, bot):
        self.bot = bot
        global instance
        instance = bot

    def checknews(self, user_id):
        res = c.execute("SELECT Sold FROM SHOP.Shop WHERE UserID=? AND Sold='Sold'", (user_id,))
        if res is not None:
            news = ""
            i = 0
            while True:
                row = res.fetchone()
                if row is None:
                    break
                if row == "Sold" or row == ('Sold',):
                    item_name = c.execute("SELECT Item FROM SHOP.Shop WHERE UserID=?", (user_id,))
                    item_name = item_name.fetchone()[i]
                    sold_for = c.execute("SELECT Price FROM SHOP.Shop WHERE Item=? AND UserID=?",
                                                 (item_name, user_id))
                    sold_for = sold_for.fetchone()[i]
                    c.execute("DELETE FROM SHOP.Shop WHERE UserID=? AND Item=? AND Price=?",
                                      (user_id, item_name, sold_for))
                    if news != "":
                        news = news + "\n"
                    news = news + str(item_name) + " sold for " + str(sold_for)
                i = i + 1
            if news == "":
                return None
            return news
        else:
            return None

    def checkshop(self, page, type):
        res = c.execute("SELECT Sold FROM SHOP.Shop WHERE Sold IS NULL OR Sold = ''")
        if res is not None:
            cap = int(page) * 20 - 1
            start = int(page) * 20 - 20
            test = c.execute("SELECT COUNT(Item) FROM SHOP.Shop WHERE Sold IS NULL OR Sold = ''")
            if test is None:
                return None
            test = test.fetchone()[0]
            test = int(test)
            if test < start:
                return None
            if test < cap:
                cap = test
            endname = []
            endprice = []
            enddescription = []
            endtype = []
            res = c.execute("SELECT Sold FROM SHOP.Shop WHERE Sold IS NULL OR Sold = ''")
            # same thing than for checkinventory
            res = res.fetchmany(test)
            for i, value in enumerate(res):
                name = c.execute("SELECT Item FROM SHOP.Shop WHERE Sold IS NULL OR Sold = ''")
                name = name.fetchmany(test)[start + i]
                price = c.execute("SELECT Price FROM SHOP.Shop WHERE Sold IS NULL OR Sold = ''")
                price = price.fetchmany(test)[start + i]
                description = c.execute("SELECT Description FROM SHOP.Shop WHERE Sold IS NULL OR Sold = ''")
                description = description.fetchmany(test)[start + i]
                greattype = c.execute("SELECT Type FROM SHOP.Shop WHERE Sold IS NULL OR Sold = ''")
                greattype = greattype.fetchmany(test)[start + i]
                list.extend(endname, name)
                list.extend(endprice, price)
                list.extend(enddescription, description)
                list.extend(endtype, greattype)
                if (start + i) >= cap:
                    break
            if type is 1:
                return endname
            elif type is 2:
                return endprice
            elif type is 3:
                return enddescription
            elif type is 4:
                return endtype
        else:
            return None

    def checkinventory(self, user_id, page, name_or_quantity):
        res = c.execute("SELECT Name FROM RPG.Inventory WHERE UserID=?", (user_id,))
        if res is not None:
            cap = page * 20 - 1
            start = page * 20 - 20
            test = c.execute("SELECT COUNT(Name) FROM RPG.Inventory WHERE UserID=?", (user_id,))
            if test is None:
                return None
            test = test.fetchone()[0]
            test = int(test)
            if test < start:
                return None
            if page == "max":
                start = 0
                cap = test
            if test < cap:
                cap = test
            endname = []
            endquantity = []
            endtype = []
            enddescription = []
            endvalue = []
            res = c.execute("SELECT Name FROM RPG.Inventory WHERE UserID=?", (user_id,))
            # repeat because else the fetches on the cursor extracts []
            res = res.fetchmany(test)
            for i, value in enumerate(res):
                name = c.execute("SELECT Name FROM RPG.Inventory WHERE UserID=?", (user_id,))
                name = name.fetchmany(test)[start + i]  # like that i don't need to cut the (' and ',) later
                quantity = c.execute("SELECT Quantity FROM RPG.Inventory WHERE UserID=?", (user_id,))
                quantity = quantity.fetchmany(test)[start + i]
                atype = c.execute("SELECT Type FROM RPG.Inventory WHERE UserID=?", (user_id,))
                atype = atype.fetchmany(test)[start + i]
                description = c.execute("SELECT Description FROM RPG.Inventory WHERE UserID=?", (user_id,))
                description = description.fetchmany(test)[start + i]
                value = c.execute("SELECT Value FROM SETTINGS.Reference WHERE Name=? AND Type=?", (name, atype))
                if value is None:
                    pass
                else:
                    value = value.fetchmany(test)[start + i]
                list.extend(endname, name)
                list.extend(endquantity, quantity)
                list.extend(endtype, atype)
                list.extend(enddescription, description)
                list.extend(endvalue, value)
                if (start + i) >= cap:
                    break
            if name_or_quantity is 1:
                return endname
            elif name_or_quantity is 2:
                return endquantity
            elif name_or_quantity is 3:
                return endtype
            elif name_or_quantity is 4:
                return enddescription
            elif name_or_quantity is 5:
                return endvalue
        else:
            return None

    async def inventory_page(self, user_id, page, message, option=False, value_check=None, send_to: discord.Member=None):
        embed = discord.Embed(title="Inventory", description="Page " + str(page) + "\n" + str(message) + "\n\u200b",
                              color=0x0000ff)
        embed.set_author(name="OwGN-Helper", url="https://discord.gg/QVxwatk",
                         icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                  "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
        names = self.checkinventory(user_id, page, 1)
        quantity = self.checkinventory(user_id, page, 2)
        thetype = self.checkinventory(user_id, page, 3)
        description = self.checkinventory(user_id, page, 4)
        the_value = self.checkinventory(user_id, page, 5)
        if names is None:
            embed.add_field(name="No items in your inventory", value="You can get some by doing quests or in the shop",
                            inline=False)
        else:
            if value_check is None:
                for i, value in enumerate(names):
                    if names[i].startswith("Custom Command-"):
                        if option is True:
                            embed.add_field(name=str(i) + " - " + str(names[i]), value="Quantity : " + str(quantity[i]),
                                            inline=True)
                        else:
                            embed.add_field(name=str(names[i]), value="Quantity : " + str(quantity[i]), inline=True)
                    else:
                        if description[i] is not None:
                            if option is True:
                                embed.add_field(
                                    name=str(i) + " - " + str(names[i]) + " (" + str(thetype[i]) + ")" + " => " + str(
                                        description[i]),
                                    value="Quantity : " + str(quantity[i]),
                                    inline=True)
                            else:
                                embed.add_field(
                                    name=str(names[i]) + " (" + str(thetype[i]) + ")" + " => " + str(description[i]),
                                    value="Quantity : " + str(quantity[i]), inline=True)
                        else:
                            if option is True:
                                embed.add_field(
                                    name=str(i) + " - " + str(names[i]) + " (" + str(thetype[i]) + ")",
                                    value="Quantity : " + str(quantity[i]),
                                    inline=True)
                            else:
                                embed.add_field(
                                    name=str(names[i]) + " (" + str(thetype[i]) + ")",
                                    value="Quantity : " + str(quantity[i]), inline=True)
                else:
                    for i, value in enumerate(names):
                        if names[i].startswith("Custom Command-"):
                            pass
                        else:
                            if description[i] is not None:
                                if option is True:
                                    embed.add_field(
                                        name=str(i) + " - " + str(names[i]) + " (" + str(thetype[i]) + ")" + " => " + str(
                                            description[i]),
                                        value="Quantity : " + str(quantity[i]+", value : "+the_value[i]),
                                        inline=True)
                                else:
                                    embed.add_field(
                                        name=str(names[i]) + " (" + str(thetype[i]) + ")" + " => " + str(description[i]),
                                        value="Quantity : " + str(quantity[i]+", value : "+the_value[i]), inline=True)
                            else:
                                if option is True:
                                    embed.add_field(
                                        name=str(i) + " - " + str(names[i]) + " (" + str(thetype[i]) + ")",
                                        value="Quantity : " + str(quantity[i]+", value : "+the_value[i]),
                                        inline=True)
                                else:
                                    embed.add_field(
                                        name=str(names[i]) + " (" + str(thetype[i]) + ")",
                                        value="Quantity : " + str(quantity[i]+", value : "+the_value[i]), inline=True)
        if the_value is None:
            if names is not None and option is True:
                embed.add_field(name="Send the number of the item to select an item", value="\u200b", inline=False)
                embed.add_field(name="If your item is not in the page, send \"next\" or \"previous\" ", value="\u200b",
                                inline=False)
            elif names is not None:
                embed.add_field(name="Send \"next\" or \"previous\" to change the page", value="\u200b", inline=False)
        embed.set_footer(text="Made by BluePhoenixGame, Shop and Inventory by ClemCa")
        if send_to is None:
            await self.bot.say(embed=embed)
        else:
            await self.bot.send_message(send_to, embed=embed)
        return names

    def sell_item(self, user_id, name, price):
        res = c.execute("""SELECT Quantity FROM RPG.Inventory WHERE UserID=? AND Name=?""", (user_id, name))
        quantity = res.fetchone()[0]
        res = c.execute("""SELECT Type FROM RPG.Inventory WHERE UserID=? AND Name=?""", (user_id, name))
        the_type = res.fetchone()[0]
        res = c.execute("""SELECT Description FROM RPG.Inventory WHERE UserID=? AND Name=?""", (user_id, name))
        description = res.fetchone()[0]
        if quantity > 1:
            quantity = quantity - 1
            c.execute("""UPDATE RPG.Inventory SET Quantity=? WHERE UserID=? AND Name=?""",
                              (quantity, user_id, name))
        else:
            c.execute("""DELETE FROM RPG.Inventory WHERE UserID=? AND Name=?""", (user_id, name))
        sold = None
        c.execute("""INSERT INTO SHOP.Shop VALUES(?,?,?,?,?,?)""",
                          (user_id, name, the_type, price, sold, description))

    async def buy_item(self, user_id, item_id, page, the_currency):
        user_id = str(user_id)
        if user_id.startswith("('"):
            user_id = user_id[2:-3]
        from currency import Currency
        from RPG import RPG
        actual_number = page * 20 - 20
        actual_number = actual_number + int(item_id)
        seller_user_id = c.execute("SELECT UserID FROM SHOP.Shop WHERE Sold IS NULL OR Sold = ''")
        seller_user_id = seller_user_id.fetchall()
        if seller_user_id is None:
            pass
        else:
            seller_user_id = seller_user_id[actual_number]
            to_add_name = c.execute("SELECT Item FROM SHOP.Shop WHERE Sold IS NULL OR Sold = ''")
            to_add_name = to_add_name.fetchall()
            seller_user_id = str(seller_user_id)
            if seller_user_id.startswith("('"):
                seller_user_id = seller_user_id[2:-3]
            if to_add_name is None:
                pass
            else:
                to_add_name = to_add_name[actual_number]
                to_add_name = str(to_add_name)
                to_add_name = to_add_name[2:-3]
            price = c.execute("SELECT Price FROM SHOP.Shop WHERE Sold IS NULL OR Sold = ''")
            price = price.fetchall()
            if price is None:
                pass
            else:
                price = price[actual_number]
                temp = []
                list.extend(temp, price)
                price = temp[0]
            to_add_type = c.execute("SELECT Type FROM SHOP.Shop WHERE Sold IS NULL OR Sold = ''")
            to_add_type = to_add_type.fetchall()
            if to_add_type is None:
                pass
            else:
                to_add_type = to_add_type[actual_number]
                temp = []
                list.extend(temp, to_add_type)
                to_add_type = temp[0]
            to_add_description = c.execute("SELECT Description FROM SHOP.Shop WHERE Sold IS NULL OR Sold = ''")
            to_add_description = to_add_description.fetchall()
            if to_add_description is None:
                pass
            else:
                to_add_description = to_add_description[actual_number]
                temp = []
                list.extend(temp, to_add_description)
                to_add_description = temp[0]
            the_currency = str(the_currency)
            to_add_quantity = 1
            if the_currency.upper() == "GOLD":
                the_currency = 1
            elif the_currency.upper() == "SILVER":
                the_currency = 2
            else:
                the_currency = 0
            actual_coins = Currency.get_coin(instance, user_id, the_currency)
            how_much_would_it_cost = Currency.get_rate(instance, Currency.get_coins_number(instance, the_currency),
                                                       Currency.get_coins_number(instance, 0))
            if how_much_would_it_cost == "even":
                how_much_would_it_cost = float(price)
            else:
                how_much_would_it_cost = float(price) * float(how_much_would_it_cost)
            if actual_coins is None:
                actual_coins = 0
            if actual_coins > how_much_would_it_cost:
                Currency.remove_coin(instance, user_id, price, the_currency)
                Currency.add_coin(instance, seller_user_id, price, the_currency)
                c.execute(
                    """UPDATE SHOP.Shop SET Sold = 'Sold' WHERE UserID=? AND Item=? AND Price=?""",
                    (seller_user_id, to_add_name, price))
                print(instance, user_id, to_add_name, to_add_quantity, to_add_type, to_add_description)
                RPG.add_item(instance, user_id, to_add_name, to_add_quantity, to_add_type, to_add_description)
                await self.bot.say("You successfully bought the item")
            else:
                await self.bot.say(
                    "You do not have enough money for that! Try a less expensive item next time! (or, little tip, "
                    "ask your friends for money, they'll love it!)")

    @commands.command(pass_context=False)
    # xD that was the source of the error: the context was set into the first parameter
    async def shop(self, page=1):
        embed = discord.Embed(title="Shop", description="Page " + str(page) + "\n\u200b", color=0x0000ff)
        embed.set_author(name="OwGN-Helper", url="https://discord.gg/QVxwatk",
                         icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
        names = self.checkshop(page, 1)
        prices = self.checkshop(page, 2)
        description = self.checkshop(page, 3)
        types = self.checkshop(page, 4)
        if names is None:
            embed.add_field(name="No items in the shop", value="Come back another time!", inline=False)
        else:
            for i, value in enumerate(names):
                if description[i] is None:
                    if types[i] is None or "Custom Command-" in types[i]:
                        embed.add_field(name=str(i) + " - " + str(names[i]), value=str(prices[i]), inline=True)
                    else:
                        embed.add_field(name=str(i) + " - " + str(names[i]) + " (" + str(types[i]) + ")",
                                        value=str(prices[i]), inline=True)
                else:
                    if types[i] is None or "Custom Command-" in types[i]:
                        embed.add_field(name=str(i) + " - " + str(names[i]) + " => " + str(description[i]),
                                        value=str(prices[i]), inline=True)
                    else:
                        embed.add_field(name=str(i) + " - " + str(names[i]) + " (" + str(types[i]) + ")" + " => " + str(
                            description[i]), value=str(prices[i]), inline=True)
        embed.add_field(name="Values are in Base OGC",
                        value="Refer to the market command for live informations about the exchange rates",
                        inline=False)
        embed.add_field(name="\u200b", value="Page " + str(page), inline=False)
        embed.set_footer(text="Made by BluePhoenixGame, Shop by ClemCa")
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def inventory(self, ctx, page=1):
        await self.inventory_page(ctx.message.author.id, page, "Take a look at your items, commands and materials",
                                  False)
        msg = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
        if msg is None:
            return
        while msg.content == "next" or msg.content == "previous":
            if msg.content == "next":
                page = page + 1
            elif msg.content == "previous":
                if page > 0:
                    page = page - 1
            await self.inventory_page(ctx.message.author.id, page, "Take a look at your items, commands and materials",
                                      False)
            msg = await self.bot.wait_for_message(timeout=30, author=ctx.message.author,
                                                  channel=ctx.message.channel)
            if msg is None:
                return

    @commands.command(pass_context=True)
    async def buy(self, ctx, item_id=None, page=None, the_type=None):
        if item_id is None:
            await self.bot.say(
                "Please send the id of the item in the shop. Syntax: 'buy id page <Gold/Silver>'\nYou can open the shop to take a look using the &shop command")
        elif page is None:
            await self.bot.say(
                "Please send the page of the item in the shop. Syntax: 'buy id page <Gold/Silver>'\nYou can open the shop to take a look using the &shop command")
        elif the_type is None:
            await self.bot.say(
                "Please send the currency you want to buy the item with. Syntax: 'buy id page <Gold/Silver>'")
        else:
            await self.buy_item(ctx.message.author.id, int(item_id), int(page), the_type)

    @commands.command(pass_context=True)
    async def sell(self, ctx):
        page = 1
        selected = False
        success = None
        while selected is False:
            name = await self.inventory_page(ctx.message.author.id, page, "Please select an item to sell", True)
            msg = await self.bot.wait_for_message(timeout=30, author=ctx.message.author, channel=ctx.message.channel)
            if msg is None:
                return
            while msg.content == "next" or msg.content == "previous":
                if msg.content == "next":
                    page = page + 1
                elif msg.content == "previous":
                    if page > 0:
                        page = page - 1
                name = await self.inventory_page(ctx.message.author.id, page, "Please select an item to sell", True)
                msg = await self.bot.wait_for_message(timeout=30, author=ctx.message.author,
                                                      channel=ctx.message.channel)
                if msg is None:
                    return
            if msg.content == "cancel":
                success = False
                pass
            elif msg.content.isdigit():
                await self.bot.say("What price are you selling it for?")
                price = await self.bot.wait_for_message(timeout=30, author=ctx.message.author,
                                                        channel=ctx.message.channel)
                if price is None:
                    return
                if price.content == "cancel":
                    success = None
                    pass
                if price.content.isdigit():
                    success = False
                    pass
                else:
                    while price.content.isdigit() is False:
                        await self.bot.say("Invalid price")
                        price = await self.bot.wait_for_message(timeout=30, author=ctx.message.author,
                                                                channel=ctx.message.channel)
                        if price is None:
                            await self.bot.say("Order timeout")
                            return
                        if price.content == "cancel":
                            success = None
                            pass
                    if price.content.isdigit():
                        success = False
                if success is False:
                    self.sell_item(ctx.message.author.id, name[int(msg.content)], price.content)
                    success = True
                    selected = True
                elif success is None:
                    await self.bot.say("Order successfully cancelled")
                    return
            else:
                await self.bot.say("Please send a valid command")
            if success is True:
                await self.bot.say("Object successfully sold on the shop")
            else:
                await self.bot.say("Order successfully cancelled")
                return

    async def on_message(self, message):
        c.execute("UPDATE SHOP.Shop SET Sold='' WHERE UserID='System' AND Sold = 'Sold'")
        if message.content.startswith("&buy"):
            return
        if message.author.id == self.bot.user.id:
            return
        else:
            checked_news = self.checknews(message.author.id)
            if checked_news is not False and checked_news is not None:
                await self.bot.send_message(message.channel, "You got some news!\n" + str(checked_news))


def setup(bot):
    bot.add_cog(Shop(bot))
