import discord
from discord.ext import commands
from logs import Logs

chat_filter = ["NIGGA", "NIGGER", "BASTARD", "FRICKIN", "FUCKING", "FUCK", "FUCKED", "FRICKING", "BITCH", "DUMBASS",
               "OMFG", "MOTHERFUCKER", "MOTHERFUCKING", "FAGGOT", "FAGGOTS", "MORON", "SHIT"]

safe_chat = "434734644504559616"

messages_sent = 0

class SafeChat:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def report(self, ctx, member: discord.Member = None, *, reason: str = None):
        if ctx.message.channel.id == safe_chat:
            if member is None or reason is None:
                return await self.bot.say("Who are you reporting and why?")
            correct_message = await self.bot.say("I am reporting {} for {}, correct?".format(member.display_name, reason))
            await self.bot.add_reaction(correct_message, "✅")
            await self.bot.add_reaction(correct_message, "❌")
            correct_reaction = await self.bot.wait_for_reaction(author=ctx.message.author, timeout=50)
            if correct_reaction is None:
                return await self.bot.say("You took to long.")
            elif correct_reaction == '✅':
                Logs.on_member_report(member, reason, ctx.message.author)
                await self.bot.say("Member was reported.")
            elif correct_reaction == '❌':
                await self.bot.say("I was wrong? Sorry. Wanna try again or did you not want to report that member?")


    async def on_message(self, message):
        if message.channel.id == safe_chat:
            global messages_sent
            messages_sent += 1
            contents = message.content.split(" ")
            for word in contents:
                if word.upper() in chat_filter:
                    SC_Warned = discord.utils.get(message.author.server.roles, name="SC-Warned")
                    SC_Banned = discord.utils.get(message.author.server.roles, name="SC-Banned")
                    if "SC-Warned" in [role.name for role in message.author.roles]:
                        await self.bot.send_message(message.channel, "You have been caught swearing and saying `{}`! You will now be banned from {}!!!".format(word, message.channel.mention))
                        await self.bot.add_roles(message.author, SC_Banned)
                        await self.bot.remove_roles(message.author, SC_Warned)
                    elif "SC-Banned" in [role.name for role in message.author.roles]:
                        await self.bot.send_message(message.channel, "You have been caught swearing and saying `{}`! Even after you got banned from this channel. You will now be kicked from {}!!!".format(word, message.author.server.name))
                        await self.bot.kick(message.author)
                    else:
                        await self.bot.send_message(message.channel, "You have been caught swearing and saying `{}`! You will now receive a warning for {}!".format(word, message.channel.mention))
                        await self.bot.add_roles(message.author, SC_Warned)
            if messages_sent == 30:
                messages_sent = 0
                await self.bot.send_message(message.channel, "If someone have broken the {} rules then you can report them with: `&report [user] [reason]` so a staff member can see it.".format(message.channel))

    async def on_message_edit(self, before, after):
        if after.channel.id == safe_chat:
            contents = after.content.split(" ")
            message = after
            for word in contents:
                if word.upper() in chat_filter:
                    SC_Warned = discord.utils.get(after.author.server.roles, name="SC-Warned")
                    SC_Banned = discord.utils.get(after.author.server.roles, name="SC-Banned")
                    if "SC-Warned" in [role.name for role in after.author.roles]:
                        await self.bot.send_message(message.channel, "You have been caught swearing and saying `{}`! You will now be banned from {}!!!".format(word, message.channel.mention))
                        await self.bot.add_roles(after.author, SC_Banned)
                        await self.bot.remove_roles(after.author, SC_Warned)
                    elif "SC-Banned" in [role.name for role in after.author.roles]:
                        await self.bot.send_message(message.channel, "You have been caught swearing and saying `{}`! Even after you got banned from this channel. You will now be kicked from {}!!!".format(word, message.author.server.name))
                        await self.bot.kick(after.author)
                    else:
                        await self.bot.send_message(message.channel, "You have been caught swearing and saying `{}`! You will now recieve a warning for {}!".format(word, message.channel.mention))
                        await self.bot.add_roles(after.author, SC_Warned)





def setup(bot):
    bot.add_cog(SafeChat(bot))