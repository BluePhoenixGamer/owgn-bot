import discord
from discord.ext import commands
import asyncio

id_list = ["260479866921549845", "398632204672172033", "302395781497749504", "344404945359077377"]

bulk_delete = 0
member_kicked = 0



class Admin():
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    @commands.cooldown(2, 180, commands.BucketType.user)
    async def DM(self, ctx, member: discord.Member = None, *, text: str = None):
        await self.bot.delete_message(ctx.message)
        if ctx.message.author.id in id_list:
            if member is None:
                dm_error = await self.bot.say("Who should i DM?")
                await asyncio.sleep(4)
                await self.bot.delete_message(dm_error)

            else:
                if text is None:
                    await self.bot.say("What should i send to: {}?".format(member.name))
                else:
                    member_user = await self.bot.get_user_info(member.id)
                    await self.bot.send_message(member_user, text + ", from {}".format(ctx.message.author.name))
                    dm_finish = await self.bot.say("Sent: " + text + ", to: {}".format(member.name))
                    await asyncio.sleep(4)
                    await self.bot.delete_message(dm_finish)
        else:
            pass

    @commands.command(pass_context=True)
    @commands.cooldown(2, 180, commands.BucketType.user)
    async def nick(self, ctx, user: discord.Member, *, nick: str):
        if ctx.message.author.id in id_list:
            await self.bot.change_nickname(user, nick)
            bot_nick = await self.bot.say("Changed the nickname to " + nick)
            await asyncio.sleep(10)
            await self.bot.delete_message(bot_nick)
        else:
            self.bot.say("You don't have permission!")

    @commands.command(pass_context=True)
    @commands.cooldown(2, 180, commands.BucketType.user)
    async def kick(self, ctx, user: discord.Member = None):
        if ctx.message.author.id in id_list:
            if user is None:
                bot_error_kick = await self.bot.say("Try again but with a mention, I can't kick air!")
                await asyncio.sleep(10)
                await self.bot.delete_message(bot_error_kick)
            else:
                global member_kicked
                member_kicked = 1
                await self.bot.say("Cya {}".format(user.name))
                await self.bot.kick(user)
                print("Kicked a member")
        else:
            self.bot.say("You don't have permission!")

    @commands.command(pass_context=True)
    @commands.cooldown(2, 180, commands.BucketType.user)
    async def ban(self, ctx, user: discord.Member = None):
        if ctx.message.author.id in id_list:
            try:
                await self.bot.ban(user)
                await self.bot.say("{}, I guess you deserve the **Ban-hammer**?!!".format(user.name))
                print("Banned a member")
            except discord.errors.Forbidden:
                await self.bot.say("I am forbidden to do that")
            await self.bot.delete_message(ctx.message)
        else:
            self.bot.say("You don't have permission")

    @commands.command(pass_context=True)
    @commands.cooldown(2, 180, commands.BucketType.user)
    async def delete(self, ctx, number: int):
        if ctx.message.author.id in id_list:
            if number > 1:
                mgs = []
                number = number + 1
                async for x in self.bot.logs_from(ctx.message.channel, limit=number):
                    mgs.append(x)
                global bulk_delete
                bulk_delete = number
                await self.bot.delete_messages(mgs)
            else:
                await self.bot.say("You know you can do that manually?")
        else:
            await self.bot.say("You don't have permission!")

    @commands.command(pass_context=True)
    @commands.cooldown(2, 180, commands.BucketType.user)
    async def mute(self, ctx, user: discord.Member = None):
        if ctx.message.author.id in id_list:
            if user is None:
                await self.bot.say("Who should i give the role: mute ?")
            else:
                add_role = discord.utils.get(ctx.message.server.roles, name="rb_mute")
                await self.bot.add_roles(user, add_role)
                await self.bot.say(":mute:{} is now muted".format(user.name))
        else:
            await self.bot.say("You don't have permission!")


def setup(bot):
    bot.add_cog(Admin(bot))
