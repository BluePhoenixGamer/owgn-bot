import time
import traceback
import sys

import discord
from discord.ext import commands


# well dunno why it didn't take the databases changes as changes
class CommandError:
    def __init__(self, bot):
        self.bot = bot

    #async def on_error(self, event, *args, **kwargs):  # actually I just have that error:
    #    # aiohttp.errors.ClientOSError: [Errno 10051] Cannot connect to host discordapp.com:443 ssl:True [Can not
    #    # connect to discordapp.com:443 [Connect call failed ('104.16.58.5', 443)]]
    #    # didn't work in the end, I need to find something that can react to all errors not only those of discord
    #    if "FORTNITE" in str(traceback.format_exc()).upper():
    #        import pfaw
    #        from pfaw import Fortnite, Platform, Mode
    #        fortnite = pfaw.Fortnite(
    #            fortnite_token='ZWM2ODRiOGM2ODdmNDc5ZmFkZWEzY2IyYWQ4M2Y1YzY6ZTFmMzFjMjExZjI4NDEzMTg2MjYyZDM3YTEzZmM4NGQ=',
    #            launcher_token='MzRhMDJjZjhmNDQxNGUyOWIxNTkyMTg3NmRhMzZmOWE6ZGFhZmJjY2M3Mzc3NDUwMzlkZmZlNTNkOTRmYzc2Y2Y=',
    #            password='Ho#F&NXbkQqkt95K5$Rw', email='fortniteapi@protonmail.com')
    #    elif "CONNECT" in str(traceback.format_exc()).upper() or "CONNECTION" in str(traceback.format_exc()).upper():
    #        time.sleep(1)  # no one wants his program to stack processes while disconnected
    #        from bot import connect  # to avoid messing up with the bot commands if I import bot
    #        from bot import play
    #        connect()
    #        print("trying to reconnect...")
    #        await play()
    #    else:
    #        print(event)
    #        print(args)
    #        print(kwargs)

    #Useless, i have made a fix for discord. and then i made a reload cog command. So if fortnite wont load then because of the api then we just reload the cog.

    async def on_command_error(self, ctx, error):
        if isinstance(ctx, discord.ext.commands.errors.MissingRequiredArgument):
            embed = discord.Embed(title="Missing Argument",
                                  description="A required argument is missing like a mention, go to the help command to"
                                              " see what arguments you need to give.",
                                  color=discord.Color.red())
            await self.bot.send_message(error.message.channel, embed=embed)

        elif isinstance(ctx, discord.ext.commands.errors.CommandNotFound):
            embed = discord.Embed(title="Command Not Found",
                                  description="That is not a command, go to the help command to see a list of commands."
                                              ,
                                  color=discord.Color.red())
            await self.bot.send_message(error.message.channel, embed=embed)

        elif isinstance(ctx, discord.ext.commands.errors.CommandOnCooldown):
            embed = discord.Embed(title="Command On Cooldown",
                                  description="That command is on a cooldown, wait a bit and try again in {}s.".format(str(ctx.args[0])[34:-4]),
                                  color=discord.Color.red())
            await self.bot.send_message(error.message.channel, embed=embed)

        elif isinstance(ctx, discord.ext.commands.errors.TooManyArguments):
            embed = discord.Embed(title="Too Many Arguments",
                                  description="You gave the command to many arguments, go to help command to see what arguments the command need",
                                  color=discord.Color.red())
            await self.bot.send_message(error.message.channel, embed=embed)

        elif isinstance(ctx, discord.ext.commands.errors.BadArgument):
            embed = discord.Embed(title="Bad Argument",
                                  description="You gave the command a wrong argument, like giving me a word when it needs a number",
                                  color=discord.Color.red())
            await self.bot.send_message(error.message.channel, embed=embed)

        elif isinstance(ctx, discord.ext.commands.errors.NoPrivateMessage):
            embed = discord.Embed(title="No Private Message",
                                  description="You cant use that command in direct message, this probaly means it needs to be used inside a server.",
                                  color=discord.Color.red())
            await self.bot.send_message(error.message.channel, embed=embed)

        elif isinstance(ctx, discord.errors.Forbidden):
            embed = discord.Embed(title="Fobidden",
                                  description="Seems like either you or me dont have permission for that.",
                                  color=discord.Color.red())
            await self.bot.send_message(error.message.channel, embed=embed)

        elif isinstance(ctx, discord.errors.InvalidArgument):
            embed = discord.Embed(title="Invalid Argument",
                                  description="You gave me an invalid argument.",
                                  color=discord.Color.red())
            await self.bot.send_message(error.message.channel, embed=embed)

        elif isinstance(ctx, discord.errors.HTTPException):
            embed = discord.Embed(title="HTTP Exception",
                                  description="Could be a error in the code. Most commonly occurs if a empty embed tries to get sent.",
                                  color=discord.Color.red())
            await self.bot.send_message(error.message.channel, embed=embed)
            #also occurs whne trying to delete messages over 14 days old.

        elif isinstance(ctx, discord.errors.NotFound):
            embed = discord.Embed(title="Not Found",
                                  description="That was not found.",
                                  color=discord.Color.red())
            await self.bot.send_message(error.message.channel, embed=embed)

        elif isinstance(ctx, discord.ext.commands.errors.DisabledCommand):
            embed = discord.Embed(title="Disabled Command",
                                  description="That is a disabled command.",
                                  color=discord.Color.red())
            await self.bot.send_message(error.message.channel, embed=embed)

        else:
            print('Ignoring exception in command {}'.format(error.command), file=sys.stderr)
            traceback.print_exception(type(ctx), ctx, ctx.__traceback__, file=sys.stderr)
            embed = discord.Embed(title="Unknown Error",
                                  description="A non registered error has occured. Please contact BluePhoenixGamer or ClemCa:\n {}".format(ctx),
                                  color=discord.Color.red())
            await self.bot.send_message(error.message.channel, embed=embed)


def setup(bot):
    bot.add_cog(CommandError(bot))
