import asyncio
import time
import re

import discord
from discord.ext import commands

all_commands = ['DM', 'dm', 'ban', 'delete', 'kick', 'nick', 'hug', 'kiss', 'coins', 'flipcoin', 'flipacoin', 'myid',
                'pay', 'xp', 'dice', 'echo', 'insult', 'magic8ball', 'remind', 'shrug', 'test', 'channelinfo',
                'emojinfo', 'faq', 'info', 'memberinfo', 'serverinfo', 'uptime', 'userinfo']

modules = ['fortnite', 'f', 'frt', 'rpg', 'RPG', 'shop', 'SHOP']

id_list = ["260479866921549845", "398632204672172033", "302395781497749504", "344404945359077377"]


class Info():
    def __init__(self, bot):
        self.bot = bot

    async def on_ready(self):
        global BootTime
        BootTime = time.perf_counter()

    def help_command(self, command, ctx):

        if command is None:
            embed = discord.Embed(title="Help", description="Page 0", color=ctx.message.author.color)
            embed.set_author(name="OwGN-Helper",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                      "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            embed.add_field(name=":question: help [page/module/command]",
                            value="I send a embed with help from a module or the basic help but you specify a page.",
                            inline=False)
            embed.set_footer(text="Made by BluePhoenixGame")
            return embed

        elif command.isnumeric():
            if int(command) == 1:
                embed = discord.Embed(title="Help", description="Page 1", color=ctx.message.author.color)
                embed.set_author(name="OwGN-Helper",
                                 url="https://discord.gg/QVxwatk",
                                 icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                          "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
                embed.add_field(name=":question: help [page/module/command]",
                                value="I send a embed with help from a command, module or the basic help but you specify a page.",
                                inline=False)
                embed.add_field(name=":scroll: userinfo [user]",
                                value="I give you info on username, id and time joined etc.", inline=False)
                embed.add_field(name=":scroll: emojinfo [emoji]", value="I give you info on name and id etc.",
                                inline=False)
                embed.add_field(name=":scroll: channelinfo [channel]",
                                value="I give you info on name, id and Time created etc.", inline=False)
                embed.add_field(name=":lips: echo [text]", value="I repeat what you say.", inline=False)
                embed.add_field(name="¯＼(º_o)/¯ shrug", value="¯＼(º_o)/¯", inline=False)
                embed.add_field(name=":game_die: dice", value="I roll a dice for you", inline=False)
                embed.add_field(name=":money_with_wings: flipcoin", value="I flip a coin for you", inline=False)
                embed.set_footer(text="Made by BluePhoenixGame")
                return embed

            elif int(command) == 2:
                embed = discord.Embed(title="Help", description="Page 2", color=ctx.message.author.color)
                embed.set_author(name="OwGN-Helper",
                                 url="https://discord.gg/QVxwatk",
                                 icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                          "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
                embed.add_field(name=":kissing_heart: kiss [user]", value="You kiss the mentioned user", inline=False)
                embed.add_field(name=":8ball: magic8ball [question]", value="Ill answer your true of false question.",
                                inline=False)
                embed.add_field(name=":sleeping: remind [minutes]",
                                value="I mention you after a defined amount of minutes and maybe remind you a sentance.",
                                inline=False)
                embed.add_field(name=":hugging: hug [user]", value="You hug the mentioned user.", inline=False)
                embed.add_field(name=":thinking: xp", value="I show you how much xp you have", inline=False)
                embed.add_field(name=":money_with_wings: coins", value="I show you how many coins you have",
                                inline=False)
                embed.add_field(name=":thinking: faq", value="I give you answers on some commonly asked questions",
                                inline=False)
                embed.add_field(name=":thinking: info", value="I give you some info on me", inline=False)
                embed.add_field(name=":thinking: uptime", value="I'll tell you for how long i've been up!.",
                                inline=False)
                embed.add_field(name=":spy: nick [user] [nick]", value="I change the mentioned user the defined name",
                                inline=False)
                embed.set_footer(text="Made by BluePhoenixGame")
                return embed

            elif int(command) == 3:
                embed = discord.Embed(title="Help", description="Page 3", color=ctx.message.author.color)
                embed.set_author(name="OwGN-Helper",
                                 url="https://discord.gg/QVxwatk",
                                 icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                          "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
                embed.add_field(name=":scroll: memberinfo [member]",
                                value="I give you info on server specific stuff like highest role.", inline=False)
                embed.add_field(name=":scissors: delete [amount]",
                                value="I delete the amount of messages in the channel you sent the command in.",
                                inline=False)
                embed.add_field(name=":moneybag: pay [user] [amount] [coin type]",
                                value="You transfer money over to the mentioned user.", inline=False)
                embed.add_field(name=":newspaper: serverinfo",
                                value="I'll tell you some info about the server you're in.", inline=False)
                embed.add_field(name=":arrow_forward: Exchange [amount] [gold/silver] [gold/silver]",
                                value="You exchánge the first type to the second", inline=False)
                embed.add_field(name=":one::four: myid", value="I'll say your id.", inline=False)
                embed.add_field(name=":fire: insult", value="I'll insult you.", inline=False)
                embed.add_field(name=":boot: kick [user]", value="I kick the mentioned user", inline=False)
                embed.add_field(name=":skull: ban [user]", value="I ban the mentioned user", inline=False)
                embed.set_footer(text="Made by BluePhoenixGame")
                return embed

            else:

                embed = discord.Embed(title="Help", description="Page " + str(command), color=ctx.message.author.color)
                embed.set_author(name="OwGN-Helper",
                                 url="https://discord.gg/QVxwatk",
                                 icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                          "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
                embed.add_field(name="No more page", value="This page is empty, please try a lower number",
                                inline=False)
                embed.set_footer(text="Made by BluePhoenixGame")
                return embed

        elif command not in modules:
            if command.isnumeric():
                temp_command = None

            elif command.isnumeric() == False:
                temp_command = command
                regex_commands = []
                search = r'{}'.format(command)

            for x in all_commands:
                regex1 = re.findall(search, x)
                if len(regex1) > 0:
                    regex_commands.append(x)

            embed = discord.Embed(title="Help", description="Command Search: " + str(command),
                                  color=ctx.message.author.color)
            embed.set_author(name="OwGN-Helper",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                      "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            for x in regex_commands:
                if temp_command == None or x == 'userinfo':
                    embed.add_field(name=":scroll: userinfo [user]",
                                    value="I give you info on username, id and time joined etc.", inline=False)
                elif x == 'dm':
                    if ctx.message.author.id in id_list:
                        embed.add_field(name=":e_mail: DM [user] [text]", value="I send the text to the user.",
                                        inline=False)

                elif x == 'delete':
                    embed.add_field(name=":scissors: delete [amount]",
                                    value="I delete the amount of messages in the channel you sent the command in.",
                                    inline=False)


                elif x == 'memberinfo':
                    embed.add_field(name=":scroll: memberinfo [member]",
                                    value="I give you info on server specific stuff like highest role.", inline=False)

                elif x == 'emojinfo':
                    embed.add_field(name=":scroll: emojinfo [emoji]", value="I give you info on name and id etc.",
                                    inline=False)

                elif x == 'channelinfo':
                    embed.add_field(name=":scroll: channelinfo [channel]",
                                    value="I give you info on name, id and Time created etc.", inline=False)

                elif x == 'echo':
                    embed.add_field(name=":lips: echo [text]", value="I repeat what you say.", inline=False)

                elif x == 'shrug':
                    embed.add_field(name="¯＼(º_o)/¯ shrug", value="¯＼(º_o)/¯", inline=False)

                elif x == 'dice':
                    embed.add_field(name=":game_die: dice", value="I roll a dice for you", inline=False)

                elif x == 'flipcoin':
                    embed.add_field(name=":money_with_wings: flipcoin", value="I flip a coin for you", inline=False)

                elif x == 'kiss':
                    embed.add_field(name=":kissing_heart: kiss [user]", value="You kiss the mentioned user",
                                    inline=False)

                elif x == 'magic8ball':
                    embed.add_field(name=":8ball: magic8ball [question]",
                                    value="Ill answer your true of false question.",
                                    inline=False)

                elif x == 'exchange':
                    embed.add_field(name=":arrow_forward: exchange [amount] [gold/silver] [gold/silver]",
                                    value="You exchange the first type to the second", inline=False)

                elif x == 'insult':
                    embed.add_field(name=":fire: insult", value="I'll insult you.", inline=False)

                elif x == 'serverinfo':
                    embed.add_field(name=":newspaper: serverinfo",
                                    value="I'll tell you some info about the server you're in.", inline=False)

                elif x == 'remind':
                    embed.add_field(name=":sleeping: remind [minutes] (text)",
                                    value="I mention you after a defined amount of minutes and maybe remind you a sentance.",
                                    inline=False)

                elif x == 'hug':
                    embed.add_field(name=":hugging: hug [user]", value="You hug the mentioned user.", inline=False)

                elif x == 'xp':
                    embed.add_field(name=":thinking: xp (user)",
                                    value="I show you how much xp you have or the user you mentioned.", inline=False)

                elif x == 'coins':
                    embed.add_field(name=":money_with_wings: coins  (user)",
                                    value="I show you how many coins you have or the user you mentioned", inline=False)

                elif x == 'pay':
                    embed.add_field(name=":moneybag: pay [user] [amount] [coin type]",
                                    value="You transfer money over to the mentioned user.", inline=False)

                elif x == 'faq':
                    embed.add_field(name=":thinking: faq", value="I give you answers on some commonly asked questions",
                                    inline=False)

                elif x == 'info':
                    embed.add_field(name=":thinking: info", value="I give you some info on me", inline=False)

                elif x == 'uptime':
                    embed.add_field(name=":thinking: uptime", value="I'll tell you for how long i've been up!.",
                                    inline=False)

                elif x == 'nick':
                    embed.add_field(name=":spy: nick [user] [nick]",
                                    value="I change the mentioned user the defined name",
                                    inline=False)
                elif x == 'myid':
                    embed.add_field(name=":one::four: myid", value="I'll say your id", inline=False)

                elif x == 'kick':
                    embed.add_field(name=":boot: kick [user]", value="I kick the mentioned user", inline=False)

                elif x == 'ban':
                    embed.add_field(name=":skull: ban [user]", value="I ban the mentioned user", inline=False)
            return embed


        elif command == "fortnite" or command == "f" or command == "frt":
            embed = discord.Embed(title="Fortnite Stats Help",
                                  description="All of the commands to the fortnite stats.",
                                  color=ctx.message.author.color)
            embed.set_author(name="Fortnite Stats",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/434617271076257821"
                                      "/7638099a14155c68de387da40d4781ee.webp?size=1024")
            embed.add_field(name="Solo", value="&fsolo [user]", inline=True)
            embed.add_field(name="Duo", value="fduo [user]", inline=True)
            embed.add_field(name="Squad", value="fsquad [user]", inline=True)
            embed.add_field(name="All", value="fall [user]", inline=True)
            embed.add_field(name="Solo Compare", value="fsoloc [user1] [user2]", inline=True)
            embed.add_field(name="Duo Compare", value="fduoc [user1] [user2]", inline=True)
            embed.add_field(name="Squad Compare", value="fsquadc [user1] [user2]", inline=True)
            embed.add_field(name="All Compare", value="fallc [user1] [user2]", inline=True)
            embed.add_field(name="News", value="fnews", inline=True)
            embed.add_field(name="Servers up?", value="&fservers", inline=True)
            embed.set_footer(text="Powered with PFAW made by nicolaskenner")
            return embed

        elif command == "RPG" or command == "rpg":  # don't want to use .upper because this cause errors if it is not str
            embed = discord.Embed(title="RPG module Help", description="Page 2", color=0xff0000)
            embed.set_author(name="OwGN-Helper",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                      "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            embed.add_field(name=":dash: The RPG module is very simple",
                            value="For now, there is only quests you have 1% of"
                                  "chances to spawn. These quests cost money,"
                                  "but have chances to returns rewards too.\n"
                                  "There'll soon be a generator for the story of"
                                  "the quests as well as other rewards.\n"
                                  "Materials are actually without use, but will"
                                  "soon have some use with crafting.")
            embed.set_footer(text="RPG module by ClemCa")
            return embed

        elif command == "shop" or command == "SHOP" or command == "Shop":
            embed = discord.Embed(title="Shop module Help", description="Page 2", color=0x0000ff)
            embed.set_author(name="OwGN-Helper",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                      "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            embed.add_field(name=":shopping_cart: shop", value="Show the shop", inline=False)
            embed.add_field(name=":briefcase: inventory", value="Let me guess... show the inventory?", inline=False)
            embed.add_field(name=":moneybag: buy [item] [page] [currency]", value="You can buy things, there are three "
                                                                                  "parameters:\nItem is the number on the "
                                                                                  "side of the item when you use the shop "
                                                                                  "command.\nPage is... the page you saw "
                                                                                  "the item in...\nAs for currency, "
                                                                                  "it's either 'Gold' or 'Silver', "
                                                                                  "the two main currencies",
                            inline=False)
            embed.add_field(name=":moneybag: sell", value="On this one you're guided by an embed. "
                                                          "~~I WASN'T too lazy to do the same for buy~~", inline=False)
            embed.add_field(name=":checkered_flag: automatic notifications", value="Little bonus! When someone buys "
                                                                                   "your item, you are notified just "
                                                                                   "after the next message you send.")
            embed.add_field(name=":chart: market", value="Get the actual state of the coins market. This means you can "
                                                         "get the values of Gold and Silver OGC compared to each other,"
                                                         " and you can easily evaluate the cost of items in Basic OGC "
                                                         "compared to these currencies", inline=False)
            embed.set_footer(text="Shop module by ClemCa")
            return embed
        else:
            embed = discord.Embed(title="Eeehhh",
                                  description="it didn't work?? Did you type the command correctly? is there really a module/command/page with number/name " + str(
                                      command))
            return embed

    @commands.command(pass_context=True)
    async def help(self, ctx, command=None):
        help_embed = self.help_command(command, ctx)
        await self.bot.say(embed=help_embed)

    @commands.command(pass_context=True)
    async def uptime(self):
        embed = discord.Embed(title="Uptime", color=discord.Color.blue())
        embed.set_author(name="OwGN-Helper",
                         url="https://discord.gg/QVxwatk",
                         icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                  "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
        uptime_seconds = int(time.perf_counter() - BootTime)
        uptime_minutes = uptime_seconds / 60
        uptime_hours = uptime_minutes / 60
        uptime_days = uptime_hours / 24

        if uptime_minutes >= 1:
            embed.add_field(name="Minutes", value=str(uptime_minutes))

        if uptime_hours >= 1:
            embed.add_field(name="Hours", value=str(uptime_hours))

        if uptime_days >= 1:
            embed.add_field(name="Days", value=str(uptime_days))
        else:
            embed.add_field(name="Seconds", value=str(uptime_seconds))
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def serverinfo(self, ctx):
        server_name = ctx.message.server.name
        server_region = ctx.message.server.region
        server_afk_timeout = ctx.message.server.afk_timeout
        server_afk_channel = ctx.message.server.afk_channel
        server_member_count = ctx.message.server.member_count
        server_icon_url = ctx.message.server.icon_url
        server_id = ctx.message.server.id
        server_owner = ctx.message.server.owner
        server_is_large = ctx.message.server.large
        server_mfa_level = ctx.message.server.mfa_level
        if server_mfa_level is 0:
            server_mfa_level = "False"
        elif server_mfa_level is 1:
            server_mfa_level = "True"
        server_created_time = ctx.message.server.created_at
        embed = discord.Embed(title="{}'s info".format(server_name),
                              description="This servers info")
        embed.set_author(name="OwGN-Helper",
                         url="https://discord.gg/QVxwatk",
                         icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
        embed.set_thumbnail(url=server_icon_url)
        embed.add_field(name="Name", value=server_name, inline=True)
        embed.add_field(name="ID", value=server_id, inline=True)
        embed.add_field(name="Region", value=server_region, inline=True)
        embed.add_field(name="Time Created", value=server_created_time, inline=True)
        embed.add_field(name="Owner", value=server_owner, inline=True)
        embed.add_field(name="Member Count", value=server_member_count, inline=True)
        embed.add_field(name="Is Large?", value=server_is_large, inline=True)
        embed.add_field(name="AFK Timeout", value=server_afk_timeout, inline=True)
        embed.add_field(name="AFK Channel", value=server_afk_channel, inline=True)
        embed.add_field(name="Admin Need 2FA?", value=server_mfa_level, inline=True)
        embed.add_field(name="Icon URL", value=server_icon_url, inline=False)
        embed.set_footer(text="Made by BluePhoenixGame")
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def memberinfo(self, ctx, member: discord.Member = None):
        if member is None:
            info_name = ctx.message.author.name
            info_nick = ctx.message.author.nick
            info_id = ctx.message.author.id
            info_status = ctx.message.author.status
            info_color = ctx.message.author.color
            info_top_role = ctx.message.author.top_role
            info_joined = ctx.message.author.joined_at
            info_game = ctx.message.author.game
            info_roles = []
            info_url = ctx.message.author.avatar_url

            for x in ctx.message.author.roles:
                info_roles.append(x.name)

            embed = discord.Embed(title="Your member info",
                                  description="Since you didn't tag anyone, you will get your own info",
                                  color=info_color)
            embed.set_author(name="OwGN-Helper",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            embed.set_thumbnail(url=info_url)
            embed.add_field(name="Name", value=info_name, inline=True)
            embed.add_field(name="Nickname", value=info_nick, inline=True)
            embed.add_field(name="ID", value=info_id, inline=True)
            embed.add_field(name="Status", value=info_status, inline=True)
            embed.add_field(name="Color", value=info_color, inline=True)
            embed.add_field(name="Highest role", value=info_top_role, inline=True)
            embed.add_field(name="Time Joined", value=info_joined, inline=True)
            embed.add_field(name="Roles", value=str(info_roles), inline=True)
            embed.add_field(name="Game Playing", value=info_game, inline=True)
            embed.add_field(name="Avatar URL", value=info_url, inline=False)
            embed.set_footer(text="Made by BluePhoenixGame")
            await self.bot.say(embed=embed)
        else:
            info_name = member.name
            info_nick = member.nick
            info_id = member.id
            info_status = member.status
            info_color = member.color
            info_top_role = member.top_role
            info_joined = member.joined_at
            info_game = member.game
            info_url = member.avatar_url
            embed = discord.Embed(title="Member info of: " + member.name,
                                  description="Here is all of " + member.name + "'s information", color=info_color)
            embed.set_author(name="OwGN-Helper",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            embed.set_thumbnail(url=info_url)
            embed.add_field(name="Name", value=info_name, inline=True)
            embed.add_field(name="Nickname", value=info_nick, inline=True)
            embed.add_field(name="ID", value=info_id, inline=True)
            embed.add_field(name="Status", value=info_status, inline=True)
            embed.add_field(name="Color", value=info_color, inline=True)
            embed.add_field(name="Highest role", value=info_top_role, inline=True)
            embed.add_field(name="Time Joined", value=info_joined, inline=True)
            embed.add_field(name="Game Playing", value=info_game, inline=True)
            embed.add_field(name="Avatar URL", value=info_url, inline=False)
            embed.set_footer(text="Made by BluePhoenixGame")
            await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def userinfo(self, ctx, user: discord.User = None):
        if user is None:
            info_name = ctx.message.author.name
            info_id = ctx.message.author.id
            info_discriminator = ctx.message.author.discriminator
            info_avatar = ctx.message.author.avatar
            info_is_bot = ctx.message.author.bot
            info_avatar_url = ctx.message.author.avatar_url
            info_default_avatar = ctx.message.author.default_avatar
            info_mention = ctx.message.author.mention
            info_created = ctx.message.author.created_at
            info_display_name = ctx.message.author.display_name
            embed = discord.Embed(title="Your user info",
                                  description="Since you didn't tag anyone, you will get your own info",
                                  color=ctx.message.author.color)
            embed.set_author(name="OwGN-Helper",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            embed.set_thumbnail(url=info_avatar_url)
            embed.add_field(name="Name", value=info_name, inline=True)
            embed.add_field(name="Display Name", value=info_display_name, inline=True)
            embed.add_field(name="ID", value=info_id, inline=True)
            embed.add_field(name="Discord Tag", value=info_discriminator, inline=True)
            embed.add_field(name="Is Bot?", value=info_is_bot, inline=True)
            embed.add_field(name="Avatar", value=info_avatar, inline=True)
            embed.add_field(name="Avatar URL", value=info_avatar_url, inline=True)
            embed.add_field(name="Default Avatar Color", value=info_default_avatar, inline=True)
            embed.add_field(name="Mention Text", value=info_mention, inline=True)
            embed.add_field(name="Time Created", value=info_created, inline=False)
            embed.set_footer(text="Made by BluePhoenixGame")
            await self.bot.say(embed=embed)
        else:
            info_name = ctx.message.author.name
            info_id = ctx.message.author.id
            info_discriminator = ctx.message.author.discriminator
            info_avatar = ctx.message.author.avatar
            info_is_bot = ctx.message.author.bot
            info_avatar_url = ctx.message.author.avatar_url
            info_default_avatar = ctx.message.author.default_avatar
            info_mention = ctx.message.author.mention
            info_created = ctx.message.author.created_at
            info_display_name = ctx.message.author.display_name
            embed = discord.Embed(title="Your user info",
                                  description="Since you didn't tag anyone, you will get your own info",
                                  color=ctx.message.author.color)
            embed.set_author(name="OwGN-Helper",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            embed.set_thumbnail(url=info_avatar_url)
            embed.add_field(name="Name", value=info_name, inline=True)
            embed.add_field(name="Display Name", value=info_display_name, inline=True)
            embed.add_field(name="ID", value=info_id, inline=True)
            embed.add_field(name="Discord Tag", value=info_discriminator, inline=True)
            embed.add_field(name="Is Bot?", value=info_is_bot, inline=True)
            embed.add_field(name="Avatar", value=info_avatar, inline=True)
            embed.add_field(name="Avatar URL", value=info_avatar_url, inline=True)
            embed.add_field(name="Default Avatar", value=info_default_avatar, inline=True)
            embed.add_field(name="Mention Text", value=info_mention, inline=True)
            embed.add_field(name="Time Created", value=info_created, inline=False)
            embed.set_footer(text="Made by BluePhoenixGame")
            await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def emojinfo(self, ctx, emoji: discord.Emoji = None):
        if emoji is None:
            bot_error_emojinfo = await self.bot.say("What emoji you want info on?")
            await asyncio.sleep(10)
            await self.bot.delete_message(bot_error_emojinfo)
        else:
            einfo_name = emoji.name
            einfo_id = emoji.id
            einfo_created = emoji.created_at
            einfo_url = emoji.url
            embed = discord.Embed(title="Emoji info on: " + emoji.name,
                                  description="Here is all of " + emoji.name + "'s information",
                                  color=ctx.message.author.color)
            embed.set_author(name="OwGN-Helper",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            embed.set_thumbnail(url=einfo_url)
            embed.add_field(name="Name", value=einfo_name, inline=True)
            embed.add_field(name="ID", value=einfo_id, inline=True)
            embed.add_field(name="Time Created", value=einfo_created, inline=True)
            embed.add_field(name="Image URL", value=einfo_url, inline=True)
            await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def channelinfo(self, ctx, channel: discord.Channel = None):

        if channel is None:
            channel = ctx.message.channel
            cinfo_name = channel.name
            cinfo_id = channel.id
            if channel.topic is '':
                cinfo_topic = "None"
            else:
                cinfo_topic = channel.topic
            cinfo_private = channel.is_private
            cinfo_type = channel.type
            cinfo_position = channel.position
            cinfo_created = channel.created_at
            embed = discord.Embed(title="Channel info on: " + channel.name,
                                  description="Here is all of " + channel.name + "'s information",
                                  color=ctx.message.author.color)
            embed.set_author(name="OwGN-Helper",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            embed.add_field(name="Name", value=cinfo_name, inline=True)
            embed.add_field(name="ID", value=cinfo_id, inline=True)
            embed.add_field(name="Topic", value=cinfo_topic, inline=True)
            embed.add_field(name="Is Private?", value=cinfo_private, inline=True)
            embed.add_field(name="Type", value=cinfo_type, inline=True)
            embed.add_field(name="Position", value=cinfo_position, inline=True)
            embed.add_field(name="Time Created", value=cinfo_created, inline=True)
            await self.bot.say(embed=embed)
        else:
            cinfo_name = channel.name
            cinfo_id = channel.id
            if channel.topic is '':
                cinfo_topic = "None"
            else:
                cinfo_topic = channel.topic
            cinfo_private = channel.is_private
            cinfo_type = channel.type
            cinfo_position = channel.position
            cinfo_created = channel.created_at
            embed = discord.Embed(title="Channel info on: " + channel.name,
                                  description="Here is all of " + channel.name + "'s information",
                                  color=ctx.message.author.color)
            embed.set_author(name="OwGN-Helper",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
            embed.add_field(name="Name", value=cinfo_name, inline=True)
            embed.add_field(name="ID", value=cinfo_id, inline=True)
            embed.add_field(name="Topic", value=cinfo_topic, inline=True)
            embed.add_field(name="Is Private?", value=cinfo_private, inline=True)
            embed.add_field(name="Type", value=cinfo_type, inline=True)
            embed.add_field(name="Position", value=cinfo_position, inline=True)
            embed.add_field(name="Time Created", value=cinfo_created, inline=True)
            await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def info(self, ctx):
        embed = discord.Embed(title="What im i?",
                              description="I'm an all purpose bot, aimed to help the OwGN Community. I'm coded in python, using discord.py async. I'm hosted on a raspberrypi.\nMy creator is BluePhoenixGame. \nFor a full list of commands use &help . Please read the &faq before you report any problems.",
                              color=discord.Color.green())
        embed.set_author(name="OwGN-Helper",
                         url="https://discord.gg/QVxwatk",
                         icon_url="https://cdn.discordapp.com/avatars/428560104321712158"
                                  "/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def faq(self, ctx):
        embed = discord.Embed(title="FAQ",
                              description="Some misstakes and how to fix them.",
                              color=ctx.message.author.color)
        embed.set_author(name="OwGN-Helper",
                         url="https://discord.gg/QVxwatk",
                         icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
        embed.add_field(name="Command isn't working",
                        value="Make sure it isnt a typo or any caps. It might also be that you give some needed info like a mention. Go to &help to see all the commands.",
                        inline=False)
        embed.add_field(name="Nick/Kick/Ban command don't work", value="Most people don't have permission to use it",
                        inline=False)
        embed.add_field(name="How to download PNG of avatar?",
                        value="Change the last part of the link from: .webp?size=1024 , to: .png?size=1024",
                        inline=False)
        await self.bot.say(embed=embed)


def setup(bot):
    bot.add_cog(Info(bot))
