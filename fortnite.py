import discord
from discord.ext import commands
import pfaw
from pfaw import Fortnite, Platform, Mode
import asyncio

fortnite = pfaw.Fortnite(
    fortnite_token='ZWM2ODRiOGM2ODdmNDc5ZmFkZWEzY2IyYWQ4M2Y1YzY6ZTFmMzFjMjExZjI4NDEzMTg2MjYyZDM3YTEzZmM4NGQ=',
    launcher_token='MzRhMDJjZjhmNDQxNGUyOWIxNTkyMTg3NmRhMzZmOWE6ZGFhZmJjY2M3Mzc3NDUwMzlkZmZlNTNkOTRmYzc2Y2Y=',
    password='Ho#F&NXbkQqkt95K5$Rw', email='fortniteapi@protonmail.com')

fstatus = fortnite.server_status()
fnews = fortnite.news()


class Fortnite():
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def fservers(self):
        if fstatus:
            await self.bot.say("Servers are currently up!")
        else:
            await self.bot.say("Servers are sadly not currently on...")

    @commands.command(pass_context=True)
    async def fsolo(self, ctx, user: str = None):
        user_s = fortnite.battle_royale_stats(username=user, platform=Platform.pc)
        embed = discord.Embed(title=user + " fortnite solo info", color=ctx.message.author.color)
        embed.set_author(name="Fortnite Stats",
                         url="https://discord.gg/QVxwatk",
                         icon_url="https://cdn.discordapp.com/avatars/434617271076257821/7638099a14155c68de387da40d4781ee.webp?size=1024")
        embed.add_field(name="Solo score", value=user_s.solo.score, inline=True)
        embed.add_field(name="Solo matches", value=user_s.solo.matches, inline=True)
        embed.add_field(name="Solo Time Played", value=str(user_s.solo.time) + " Min", inline=True)
        embed.add_field(name="Solo Kills", value=user_s.solo.kills, inline=True)
        embed.add_field(name="Solo wins", value=user_s.solo.wins, inline=True)
        embed.add_field(name="Solo times top 25", value=user_s.solo.top25, inline=True)
        embed.set_footer(text="Powered with PFAW made by nicolaskenner")
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def fduo(self, ctx, user: str = None):
        if user is None:
            duo_error_who = await self.bot.say("Who you want duo info on")
            await asyncio.sleep(5)
            await self.bot.delete_message(duo_error_who)
        else:
            user_duo = fortnite.battle_royale_stats(username=user, platform='pc')
            embed = discord.Embed(title=user + " fortnite duo info", color=ctx.message.author.color)
            embed.set_author(name="Fortnite Stats",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/434617271076257821/7638099a14155c68de387da40d4781ee.webp?size=1024")
            embed.add_field(name="Duo score", value=user_duo.duo.score, inline=True)
            embed.add_field(name="Duo matches", value=user_duo.duo.matches, inline=True)
            embed.add_field(name="Duo Time Played", value=str(user_duo.duo.time) + " Min", inline=True)
            embed.add_field(name="Duo Kills", value=user_duo.duo.kills, inline=True)
            embed.add_field(name="Duo wins", value=user_duo.duo.wins, inline=True)
            embed.add_field(name="Duo times top 25", value=user_duo.duo.top25, inline=True)
            embed.set_footer(text="Powered with PFAW made by nicolaskenner")
            await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def fsquad(self, ctx, user: str = None):
        if user is None:
            squad_error_who = await self.bot.say("Who you want duo info on")
            await asyncio.sleep(5)
            await self.bot.delete_message(squad_error_who)
        else:
            user_squad = fortnite.battle_royale_stats(username=user, platform='pc')
            embed = discord.Embed(title=user + " fortnite squad info", color=ctx.message.author.color)
            embed.set_author(name="Fortnite Stats",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/434617271076257821/7638099a14155c68de387da40d4781ee.webp?size=1024")
            embed.add_field(name="Squad score", value=user_squad.squad.score, inline=True)
            embed.add_field(name="Squad matches", value=user_squad.squad.matches, inline=True)
            embed.add_field(name="Squad Time Played", value=str(user_squad.squad.time) + " Min", inline=True)
            embed.add_field(name="Squad Kills", value=user_squad.squad.kills, inline=True)
            embed.add_field(name="Squad wins", value=user_squad.squad.wins, inline=True)
            embed.add_field(name="Squad times top 25", value=user_squad.squad.top25, inline=True)
            embed.set_footer(text="Powered with PFAW made by nicolaskenner")
            await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def fall(self, ctx, user: str = None):
        if user is None:
            squad_error_who = await self.bot.say("Who you want duo info on")
            await asyncio.sleep(5)
            await self.bot.delete_message(squad_error_who)
        else:
            user_squad = fortnite.battle_royale_stats(username=user, platform='pc')
            embed = discord.Embed(title=user + " all info", color=ctx.message.author.color)
            embed.set_author(name="Fortnite Stats",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/434617271076257821/7638099a14155c68de387da40d4781ee.webp?size=1024")
            embed.add_field(name="All score", value=user_squad.all.score, inline=True)
            embed.add_field(name="All matches", value=user_squad.all.matches, inline=True)
            embed.add_field(name="All Time Played", value=str(user_squad.all.time) + " Min", inline=True)
            embed.add_field(name="All Kills", value=user_squad.all.kills, inline=True)
            embed.add_field(name="All wins", value=user_squad.all.wins, inline=True)
            embed.add_field(name="All times top 25", value=user_squad.all.top25, inline=True)
            embed.set_footer(text="Powered with PFAW made by nicolaskenner")
            await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def fsoloc(self, ctx, user1: str = None, user2: str = None):
        if user1 is None or user2 is None:
            solo_error_who = await self.bot.say("Please say 2 usernames")
            await asyncio.sleep(5)
            await self.bot.delete_message(solo_error_who)
        else:
            user_s1 = fortnite.battle_royale_stats(username=user1, platform=Platform.pc)
            user_s2 = fortnite.battle_royale_stats(username=user2, platform=Platform.pc)
            embed = discord.Embed(title=user1 + " VS " + user2, color=ctx.message.author.color)
            embed.set_author(name="Fortnite Stats",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/434617271076257821/7638099a14155c68de387da40d4781ee.webp?size=1024")
            embed.add_field(name="Solo score", value=str(user_s1.solo.score) + " VS " + str(user_s2.solo.score),
                            inline=True)
            embed.add_field(name="Solo matches", value=str(user_s1.solo.matches) + " VS " + str(user_s2.solo.matches),
                            inline=True)
            embed.add_field(name="Solo Time Played",
                            value=str(user_s1.solo.time) + " VS " + str(user_s2.solo.time) + " Min", inline=True)
            embed.add_field(name="Solo Kills", value=str(user_s1.solo.kills) + " VS " + str(user_s2.solo.kills),
                            inline=True)
            embed.add_field(name="Solo wins", value=str(user_s1.solo.wins) + " VS " + str(user_s2.solo.wins),
                            inline=True)
            embed.add_field(name="Solo times top 25", value=str(user_s1.solo.top25) + " VS " + str(user_s2.solo.top25),
                            inline=True)
            embed.set_footer(text="Powered with PFAW made by nicolaskenner")
            await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def fduoc(self, ctx, user1: str = None, user2: str = None):
        if user1 is None or user2 is None:
            duo_error_who = await self.bot.say("Please say 2 usernames")
            await asyncio.sleep(5)
            await self.bot.delete_message(duo_error_who)
        else:
            user_s1 = fortnite.battle_royale_stats(username=user1, platform=Platform.pc)
            user_s2 = fortnite.battle_royale_stats(username=user2, platform=Platform.pc)
            embed = discord.Embed(title=user1 + " VS " + user2, color=ctx.message.author.id)
            embed.set_author(name="Fortnite Stats",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/434617271076257821/7638099a14155c68de387da40d4781ee.webp?size=1024")
            embed.add_field(name="Duo score", value=str(user_s1.duo.score) + " VS " + str(user_s2.duo.score),
                            inline=True)
            embed.add_field(name="Duo matches", value=str(user_s1.duo.matches) + " VS " + str(user_s2.duo.matches),
                            inline=True)
            embed.add_field(name="Duo Time Played",
                            value=str(user_s1.duo.time) + " VS " + str(user_s2.duo.time) + " Min", inline=True)
            embed.add_field(name="Duo Kills", value=str(user_s1.duo.kills) + " VS " + str(user_s2.duo.kills),
                            inline=True)
            embed.add_field(name="Duo wins", value=str(user_s1.duo.wins) + " VS " + str(user_s2.duo.wins), inline=True)
            embed.add_field(name="Duo times top 25", value=str(user_s1.duo.top25) + " VS " + str(user_s2.duo.top25),
                            inline=True)
            embed.set_footer(text="Powered with PFAW made by nicolaskenner")
            await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def fsquadc(self, ctx, user1: str = None, user2: str = None):
        if user1 is None or user2 is None:
            squad_error_who = await self.bot.say("Please say 2 usernames")
            await asyncio.sleep(5)
            await self.bot.delete_message(squad_error_who)
        else:
            user_s1 = fortnite.battle_royale_stats(username=user1, platform=Platform.pc)
            user_s2 = fortnite.battle_royale_stats(username=user2, platform=Platform.pc)
            embed = discord.Embed(title=user1 + " VS " + user2, color=ctx.message.author.color)
            embed.set_author(name="Fortnite Stats",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/434617271076257821/7638099a14155c68de387da40d4781ee.webp?size=1024")
            embed.add_field(name="Squad score", value=str(user_s1.squad.score) + " VS " + str(user_s2.squad.score),
                            inline=True)
            embed.add_field(name="Squad matches",
                            value=str(user_s1.squad.matches) + " VS " + str(user_s2.squad.matches), inline=True)
            embed.add_field(name="Squad Time Played",
                            value=str(user_s1.squad.time) + " VS " + str(user_s2.squad.time) + " Min", inline=True)
            embed.add_field(name="Squad Kills", value=str(user_s1.squad.kills) + " VS " + str(user_s2.squad.kills),
                            inline=True)
            embed.add_field(name="Squad wins", value=str(user_s1.squad.wins) + " VS " + str(user_s2.squad.wins),
                            inline=True)
            embed.add_field(name="Squad times top 25",
                            value=str(user_s1.squad.top25) + " VS " + str(user_s2.squad.top25), inline=True)
            embed.set_footer(text="Powered with PFAW made by nicolaskenner")
            await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def fallc(self, ctx, user1: str = None, user2: str = None):
        if user1 is None or user2 is None:
            all_error_who = await self.bot.say("Please say 2 usernames")
            await asyncio.sleep(5)
            await self.bot.delete_message(all_error_who)
        else:
            user_s1 = fortnite.battle_royale_stats(username=user1, platform=Platform.pc)
            user_s2 = fortnite.battle_royale_stats(username=user2, platform=Platform.pc)
            embed = discord.Embed(title=user1 + " VS " + user2, color=ctx.message.author.color)
            embed.set_author(name="Fortnite Stats",
                             url="https://discord.gg/QVxwatk",
                             icon_url="https://cdn.discordapp.com/avatars/434617271076257821/7638099a14155c68de387da40d4781ee.webp?size=1024")
            embed.add_field(name="All score", value=str(user_s1.all.score) + " VS " + str(user_s2.all.score),
                            inline=True)
            embed.add_field(name="All matches", value=str(user_s1.all.matches) + " VS " + str(user_s2.all.matches),
                            inline=True)
            embed.add_field(name="All Time Played",
                            value=str(user_s1.all.time) + " VS " + str(user_s2.all.time) + " Min", inline=True)
            embed.add_field(name="All Kills", value=str(user_s1.all.kills) + " VS " + str(user_s2.all.kills),
                            inline=True)
            embed.add_field(name="All wins", value=str(user_s1.all.wins) + " VS " + str(user_s2.all.wins), inline=True)
            embed.add_field(name="All times top 25", value=str(user_s1.all.top25) + " VS " + str(user_s2.all.top25),
                            inline=True)
            embed.set_footer(text="Powered with PFAW made by nicolaskenner")
            await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def fnews(self, ctx):
        if fnews.br != None:
            for news_message in fnews.br:
                nimage = news_message.image
                ntitle = news_message.title
                nbody = news_message.body
                embed = discord.Embed(title=ntitle, description=nbody, color=ctx.message.author.color)
                embed.set_author(name="Fortnite Stats",
                                 icon_url="https://cdn.discordapp.com/avatars/434617271076257821/7638099a14155c68de387da40d4781ee.webp?size=1024")
                embed.set_footer(text="Powered with PFAW made by nicolaskenner")
                embed.set_thumbnail(url=nimage)
                await self.bot.say(embed=embed)
        else:
            await self.bot.say("There are currently no news...")


def setup(bot):
    bot.add_cog(Fortnite(bot))
