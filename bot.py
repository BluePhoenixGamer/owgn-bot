# OwGN-Helper by BluePhoenixGame
import discord
from discord.ext import commands
import asyncio
import time

bot = commands.Bot(command_prefix=commands.when_mentioned_or('&'))
bot.remove_command('help')

online = False
startup_extensions = ["fun", "info", "admin", "hidden", "cuddle", "currency", "fortnite", "logs", "RPG", "shop",
                      "command_error", "custom_commands", "active"]


async def play():
    await bot.change_presence(game=discord.Game(name="Version 1.1"))
    game = 1
    sec = 0
    mins = 0
    hours = 0
    days = 0
    while True:
        sec = sec + 1
        await asyncio.sleep(1)
        if sec > 59:
            sec = sec - 60
            mins = mins + 1
        if mins > 59:
            mins = mins - 60
            hours = hours + 1
        if hours > 23:
            hours = hours - 24
            days = days + 1
        if game == 1 and sec % 16 == 0:
            await bot.change_presence(game=discord.Game(name="Version 1.1"))
            game = 2
        elif game == 2 and sec % 16 == 0:
            await bot.change_presence(game=discord.Game(name="Need help? Use &help!"))
            game = 3
        elif game == 3 and sec % 16 == 0:
            await bot.change_presence(game=discord.Game(name="Our World Gaming Now"))
            game = 4
        elif game == 4 and sec % 16 == 0:
            await bot.change_presence(game=discord.Game(
                name="Bot have been on for " + str(days) + " days, " + str(hours) + " hours, " + str(
                    mins) + " minutes"))
            game = 1

@bot.event
async def on_ready():
    global online
    online = True
    print("I'm running on " + bot.user.name)
    print("With the ID:" + bot.user.id)
    await play()


@bot.event
async def on_server_join(server):
    embed = discord.Embed(title="Hello!, thank you for inviting me.", description="I'm an all purpose bot"
                            ", aimed to help the OwGN Community.\nI'm coded in python and i'm hosted on a raspberrypi."
                            "\nMy creator is BluePhoenixGame, but my RPG part was made by ClemCaↃ𝖑𝖊𝖒Ↄค⠠⠉⠇⠑⠍⠠⠉⠁.\n"
                            "For a full list of commands use following command: &help ."
                            "\n Please read the FAQ by using the command: &faq  ,if you have any problems.")
    await bot.send_message(server.owner, embed)
    for channel in server.channels:
        try:
            await bot.send_message(channel, embed)
            break
        except discord.errors.Forbidden:
            pass


@bot.command()
async def load(cog: str):
    try:
        bot.load_extension(cog)
    except (AttributeError, ImportError) as e:
        await bot.say("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    await bot.say("Loaded {}.".format(cog))


@bot.command()
async def unload(cog: str):
    bot.unload_extension(cog)
    await bot.say("Unloaded {}.".format(cog))


@bot.command()
async def reload(cog: str):
    bot.unload_extension(cog)
    print("Unloaded {}".format(cog))
    try:
        bot.load_extension(cog)
        print("loaded {}".format(cog))
    except (AttributeError, ImportError) as e:
        await bot.say("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    print("Reload of {} was successful.")
    await bot.say("Reloaded {} successfully.".format(cog))


if __name__ == "__main__":
    for extension in startup_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load {}\n{}'.format(extension, exc))

TOKEN = "NDM3NjA0NTAzMjk5MDMxMDQw.DhAL-Q.XVOb4cdfNo15flDRfRkLRP6j8t4"

try:
    bot.run(TOKEN)
except:
    while online == False:
        print("A error occured while trying to run the bot and it stopped.")
        print("Reconnecting...")
        bot.run(TOKEN)
        print("Reconnect failed, trying again in 15 seconds.")
        time.sleep(15)

# owgn helper: NDI4NTYwMTA0MzIxNzEyMTU4.DggZ6A.nAu16o1Z6yet2tV6mwbgsyLMHT8
# owgn testerr: NDU2ODQwMjkyMTQyODA5MDk5.DgQZtg.Ig5eRLis1VmbkMKvFQ6tP8BhFR8
# owgn tester: NDM3NjA0NTAzMjk5MDMxMDQw.DhAL-Q.XVOb4cdfNo15flDRfRkLRP6j8t4

# i put it there for it to be easier to change, check command_error.py to know what it is
