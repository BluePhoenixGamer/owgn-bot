import discord
import re
import asyncio
import admin

log_id = '422809058060533761'


class Logs:
    def __init__(self, bot):
        self.bot = bot

    async def on_message_delete(self, message):
        log_channel = self.bot.get_channel(log_id)
        if admin.bulk_delete > 0:
            admin.bulk_delete = admin.bulk_delete - 1
            if admin.bulk_delete < 1:
                embed = discord.Embed(title="Messages Deleted", description="Messages have been deleted",
                                      color=discord.Color.orange())
                embed.set_thumbnail(url=message.author.avatar_url)
                embed.add_field(name="Channel", value=message.channel.mention, inline=True)
                embed.add_field(name="Messages", value=str(message.content[8:]), inline=True)
                await self.bot.send_message(log_channel, embed=embed)
            return

        embed = discord.Embed(title="Message Deleted", description="A message has been deleted",
                              color=discord.Color.orange())
        embed.set_thumbnail(url=message.author.avatar_url)
        embed.add_field(name="Author", value=message.author.mention, inline=True)
        embed.add_field(name="Channel", value=message.channel.mention, inline=True)
        embed.add_field(name="Content", value=message.content, inline=True)
        await self.bot.send_message(log_channel, embed=embed)

    async def on_channel_delete(self, channel):
        log_channel = self.bot.get_channel(log_id)
        embed = discord.Embed(title="Channel Deleted", description="A channel has been deleted",
                              color=discord.Color.dark_orange())
        embed.add_field(name="Name", value=channel.name, inline=True)
        embed.add_field(name="ID", value=channel.id, inline=True)
        if channel.topic is '':
            embed.add_field(name="Topic", value="None", inline=True)
        else:
            embed.add_field(name="Topic", value=channel.topic, inline=True)
        embed.add_field(name="Is Private?", value=channel.is_private, inline=True)
        embed.add_field(name="Type", value=channel.type, inline=True)
        embed.add_field(name="Time Created", value=channel.created_at, inline=True)
        await self.bot.send_message(log_channel, embed=embed)

    async def on_channel_create(self, channel):
        if channel.is_private is False:
            log_channel = self.bot.get_channel(log_id)
            embed = discord.Embed(title="Channel Created", description="A channel has been created",
                                  color=discord.Color.green())
            embed.add_field(name="Name", value=channel.name, inline=True)
            embed.add_field(name="ID", value=channel.id, inline=True)
            if channel.topic is None:
                embed.add_field(name="Topic", value="None", inline=True)
            else:
                embed.add_field(name="Topic", value=channel.topic, inline=True)
            embed.add_field(name="Is Private?", value=channel.is_private, inline=True)
            embed.add_field(name="Type", value=channel.type, inline=True)
            embed.add_field(name="Time Created", value=channel.created_at, inline=True)
            await self.bot.send_message(log_channel, embed=embed)

    async def on_channel_update(self, before, after):
        if before.topic != after.topic or before.name != after.name or before.position != after.position or before.is_private != after.is_private or before.user_limit != after.roles:
            log_channel = self.bot.get_channel(log_id)
            embed = discord.Embed(title="Channel Updated", description="A channel has been updated",
                                  color=discord.Color.dark_green())
            embed.add_field(name="Channel", value=after.name)
            if before.topic is '':
                before.topic = "None"
            if before.name != after.name:
                embed.add_field(name="Name", value=before.name + " to " + after.name)
            elif before.topic != after.topic:
                embed.add_field(name="Topic", value=before.topic + " to " + after.topic)
            elif before.position != after.position:
                embed.add_field(name="Position", value=str(before.position) + " to " + str(after.position))
            elif before.is_private != after.is_private:
                embed.add_field(name="Is Private?", value=before.is_private + " to " + after.is_private)
            elif before.user_limit != after.user_limit:
                if after.type == "voice":
                    embed.add_field(name="User Limit", value=before.user_limit + " to " + after.user_limit)
            await self.bot.send_message(log_channel, embed=embed)

    async def on_member_update(self, before, after):
        log_channel = self.bot.get_channel(log_id)
        if before.roles != after.roles:

            if len(before.roles) > len(after.roles):
                change = "removed"

            elif len(before.roles) < len(after.roles):
                change = "added"

            if change == "removed":
                role_list = before.roles
            elif change == "added":
                role_list = after.roles

            if len(before.roles) + len(after.roles) == 3:
                position = 'pass'
            else:
                for i, value in enumerate(role_list):
                    check1 = before.roles[i]
                    check2 = after.roles[i]
                    if check1 != check2:
                        position = i
                        break

            if change == "added":
                if position == 'pass':
                    pass
                else:
                    embed = discord.Embed(title="Role Added", description="A member has gained a role",
                                          color=discord.Color.blue())
                    embed.add_field(name="Member", value=after.display_name)
                    embed.add_field(name="Role", value=after.roles[position])
                    embed.set_thumbnail(url=before.avatar_url)
                    await self.bot.send_message(log_channel, embed=embed)

            elif change == "removed":
                if position == 'pass':
                    pass
                else:
                    embed = discord.Embed(title="Role Removed", description="A member has lost a role",
                                          color=discord.Color.dark_blue())
                    embed.add_field(name="Member", value=after.display_name)
                    embed.add_field(name="Role", value=before.roles[position])
                    embed.set_thumbnail(url=before.avatar_url)
                    await self.bot.send_message(log_channel, embed=embed)

        elif before.nick != after.nick:
            if before.nick is None:
                before.nick = "None"
            if after.nick is None:
                after.nick = "None"
            embed = discord.Embed(title="Nickname Changed", description="A member has a new nickname",
                                  color=discord.Color.blue())
            embed.add_field(name="Member", value=after.mention)
            embed.add_field(name="NickName", value=before.nick + " to " + after.nick)
            embed.set_thumbnail(url=before.avatar_url)
            await self.bot.say(embed=embed)

        elif before.avatar != after.avatar:
            embed = discord.Embed(title="Avatar Updated", description="A member's avatar has been updated",
                                  color=discord.Color.teal())
            embed.set_image(url=after.avatar_url)
            embed.add_field(name="User", value=after.mention)
            embed.add_field(name="Avatar URL", value=after.avatar_url)
            embed.set_thumbnail(url=before.avatar_url)
            await self.bot.say(embed=embed)
        else:
            pass

    async def on_server_role_update(self, before, after):
        log_channel = self.bot.get_channel(log_id)
        embed = discord.Embed(title="Role Updated", description="A role has been updated",
                              color=discord.Color.dark_green())
        if before.name != after.name:
            embed.add_field(name="New Name", value=after.name)
        elif before.topic != after.topic:
            embed.add_field(name="New Topic", value=after.topic)
        elif before.position != after.position:
            embed.add_field(name="New Position", value=after.position)
        elif before.is_private != after.is_private:
            embed.add_field(name="Is Now Private?", value=after.is_private)
        elif before.user_limit != after.user_limit:
            embed.add_field(name="New User Limit", value=after.user_limit)
        await self.bot.send_message(log_channel, embed=embed)

    async def on_member_join(self, member):
        log_channel = self.bot.get_channel(log_id)
        embed = discord.Embed(title="Member Joined", description="A member just joined OwGN",
                              color=discord.Color.green())
        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name="Member Mention", value=member.mention, inline=True)
        embed.add_field(name="Member Name", value=member.name, inline=True)
        embed.set_thumbnail(url=member.avatar_url)
        await self.bot.send_message(log_channel, embed=embed)

    async def on_member_remove(self, member):
        log_channel = self.bot.get_channel(log_id)
        if admin.member_kicked > 0:
            admin.member_kicked = admin.member_kicked - 1
            embed = discord.Embed(title="Member Kicked", description="A member have been kicked from OwGN",
                                  color=discord.Color.dark_orange())
            embed.set_thumbnail(url=member.avatar_url)
            embed.add_field(name="Mention", value=member.mention, inline=True)
            embed.add_field(name="Name", value=member.name, inline=True)
            embed.add_field(name="ID", value=member.id)
            embed.set_thumbnail(url=member.avatar_url)
        else:
            embed = discord.Embed(title="Member Leaved", description="A member have leaved OwGN",
                              color=discord.Color.dark_orange())
            embed.set_thumbnail(url=member.avatar_url)
            embed.add_field(name="Mention", value=member.mention, inline=True)
            embed.add_field(name="Name", value=member.name, inline=True)
            embed.add_field(name="ID", value=member.id)
            embed.set_thumbnail(url=member.avatar_url)
        await self.bot.send_message(log_channel, embed=embed)

    async def on_server_role_create(self, role):
        log_channel = self.bot.get_channel(log_id)
        embed = discord.Embed(title="Role Created", description="A new role have been created",
                              color=discord.Color.green())
        embed.add_field(name="Role", value=role.mention)
        embed.add_field(name="Role Color", value=role.colour)
        await self.bot.send_message(log_channel, embed=embed)

    async def on_server_role_remove(self, role):
        log_channel = self.bot.get_channel(log_id)
        embed = discord.Embed(title="Role Deleted", description="A role have been deleted",
                              color=discord.Color.dark_orange())
        embed.add_field(name="Role", value=role.mention)
        embed.add_field(name="Role Color", value=role.colour)
        await self.bot.send_message(log_channel, embed=embed)

    async def on_member_ban(self, member):
        log_channel = self.bot.get_channel(log_id)
        embed = discord.Embed(title="Member Banned", description="A member have been banned", color=discord.Color.red())
        embed.add_field(name="Member", value=member.display_name)
        embed.add_field(name="Id", value=member.id)
        embed.set_thumbnail(url=member.avatar_url)
        await self.bot.send_message(log_channel, embed=embed)

    async def on_member_unban(self, server, user):
        log_channel = self.bot.get_channel(log_id)
        embed = discord.Embed(title="Member Unbanned", description="A member have been unbanned",
                              color=discord.Color.dark_green())
        embed.add_field(name="Member", value=user.name)
        embed.add_field(name="ID", value=user.id)
        embed.set_thumbnail(url=user.avatar_url)
        await self.bot.send_message(log_channel, embed=embed)

    async def on_member_report(self, member: discord.Member, reason: str, reporter: discord.Member):
        log_channel = self.bot.get_channel(log_id)
        embed = discord.Embed(title="Member reported", description="A member was reported by another member in safe chat.")
        embed.add_field(name="Member", value=member.display_name)
        embed.add_field(name="Reason", value=reason)
        embed.add_field(name="Member Reporting", value=reporter)
        embed.set_thumbnail(url=member.avatar_url)
        await self.bot.send_message(log_channel, embed=embed)

    async def on_link_post(self, invite_id: str, message: str):
        log_channel = self.bot.get_channel(log_id)
        invite_info = await self.bot.get_invite(invite_id)
        embed = discord.Embed(title="Invite Posted", description="A member has posted a invite.",
                              color=discord.Color.dark_orange())
        embed.add_field(name="Member", value=message.author.display_name)
        embed.add_field(name="Message", value=message.content)
        embed.add_field(name="Channel", value=message.channel.mention)
        embed.add_field(name="Invite", value=invite_info.url)
        embed.add_field(name="Server", value=invite_info.server.name)
        embed.add_field(name="Inviter", value=str(invite_info.inviter))
        await self.bot.send_message(log_channel, embed=embed)

    async def on_message(self, message):
        text = message.content
        regex = re.findall(r'discord\.gg\/(\w+)', text)
        if len(regex) != 0:
            await self.bot.delete_message(message)
            await self.on_link_post(regex[0], message)


def setup(bot):
    bot.add_cog(Logs(bot))
