import random
import sqlite3
import discord
from discord.ext import commands

conn = sqlite3.connect('users.db', isolation_level=None)
c = conn.cursor()
c.execute("ATTACH DATABASE 'rpg.db' AS RPG")
c.execute("ATTACH DATABASE 'shop.db' AS SHOP")
c.execute("ATTACH DATABASE 'settings.db' AS SETTINGS")
c.execute("""CREATE TABLE IF NOT EXISTS Users(
                      UserID TEXT,
                      Coins FLOAT,
                      SecondCoins FLOAT,
                      Xp INTEGER,
                      Online TEXT,
                      Party TEXT,
                      MyGroup TEXT,
                      Guild TEXT,
                      PRIMARY KEY(UserID))""")
c.execute("""CREATE TABLE IF NOT EXISTS RPG.Inventory(
                      UserID TEXT,
                      Type TEXT,
                      Name TEXT,
                      Quantity INT)""")
c.execute("""CREATE TABLE IF NOT EXISTS SHOP.Shop(
                      UserID TEXT,
                      Item TEXT,
                      Type TEXT,
                      Price FLOAT,
                      Sold TEXT)""")


class Currency:
    def __init__(self, bot):
        self.bot = bot
        global instance
        instance = bot

    def create_user_if_not_exists(self, user_id):
        user_id = str(user_id)
        if user_id.startswith("('"):
            user_id = user_id[2:-3]
        res = c.execute("SELECT COUNT(*) FROM Users WHERE UserID=?", (user_id,))
        user_count = res.fetchone()[0]
        if user_count < 1:
            print("Creating user with id " + str(user_id))
            c.execute("INSERT INTO Users VALUES (?, ?, ?, ?, ?, ?, ?)", (user_id, 0, 0, 0, "", "", ""))
        res = c.execute("SELECT COUNT(*) FROM RPG.Inventory WHERE UserID=?", (user_id,))
        user_count = res.fetchone()[0]
        if user_count < 1:
            print("Adding basic item in inventory of id " + str(user_id))
            c.execute("INSERT INTO RPG.Inventory VALUES (?, ?, ?, ?, ?)", (user_id, "Access Token",
                                                                                "Basic access to all commands", "1", None))
        conn.commit()

    def get_xp(self, user_id):
        Currency.create_user_if_not_exists(instance, user_id)
        res = c.execute("SELECT Xp FROM Users WHERE UserID=?", (user_id,))
        user_xp = int(res.fetchone()[0])
        conn.commit()
        return user_xp

    def add_xp(self, user_id, amount: int):
        xp = int(self.get_xp(user_id) + amount)
        c.execute("UPDATE Users SET Xp=? WHERE UserID=?", (xp, user_id))
        conn.commit()
        return

    def remove_xp(self, user_id, amount: int):
        xp = int(self.get_xp(user_id) - amount)
        c.execute("UPDATE Users SET Xp=? WHERE UserID=?", (xp, user_id))
        conn.commit()
        return

    def get_coins_number(self, the_type: int = 0):
        if the_type == 0:
            res = c.execute("SELECT SUM(Coins) FROM Users")
            totalgold = int(res.fetchone()[0])
            res = c.execute("SELECT SUM(SecondCoins) FROM Users")
            totalsilver = int(res.fetchone()[0])
            averagetotal = [totalgold, totalsilver]
            averagetotal = float(sum(averagetotal)) / max(len(averagetotal), 1)
            return averagetotal
        if the_type == 1:
            res = c.execute("SELECT SUM(Coins) FROM Users")
            totalcoins = int(res.fetchone()[0])
            return totalcoins
        elif the_type == 2:
            res = c.execute("SELECT SUM(SecondCoins) FROM Users")
            totalcoins = int(res.fetchone()[0])
            return totalcoins

    def get_rate(self, first,
                 second):  # to get the value actual value of the money, use 0 as first and the money id as second
        if first > second or second > first:
            rate = first / second
        else:
            rate = "even"
        return rate

    def get_coin(self, user_id, the_type: int = 1):
        user_id = str(user_id)
        if user_id.startswith("('"):
            user_id = user_id[2:-3]
        if the_type == 1:
            Currency.create_user_if_not_exists(instance, user_id)
            res = c.execute("SELECT Coins FROM Users WHERE UserID=?", (user_id,))
            user_coins = int(res.fetchone()[0])
            return user_coins
        elif the_type == 2:
            Currency.create_user_if_not_exists(instance, user_id)
            res = c.execute("SELECT SecondCoins FROM Users WHERE UserID=?", (user_id,))
            user_coins = int(res.fetchone()[0])
            return user_coins

    def add_coin(self, user_id, amount: float, the_type: int = 1):
        user_id = str(user_id)
        if user_id.startswith("('"):
            user_id = user_id[2:-3]
        if the_type == 1:
            coins = int(Currency.get_coin(instance, user_id, 1) + amount)
            c.execute("UPDATE Users SET Coins=? WHERE UserID=?", (coins, user_id))
            conn.commit()
            return coins
        elif the_type == 2:
            coins = int(Currency.get_coin(instance, user_id, 2) + amount)
            c.execute("UPDATE Users SET SecondCoins=? WHERE UserID=?", (coins, user_id))
            conn.commit()
            return coins

    def remove_coin(self, user_id, amount: float, the_type: int = 1):
        if the_type == 1:
            coins = int(Currency.get_coin(instance, user_id, the_type) - amount)
            c.execute("UPDATE Users SET Coins=? WHERE UserID=?", (coins, user_id))
            conn.commit()
            return coins
        elif the_type == 2:
            coins = int(Currency.get_coin(instance, user_id, the_type) - amount)
            c.execute("UPDATE Users SET SecondCoins=? WHERE UserID=?", (coins, user_id))
            conn.commit()
            return coins

    @commands.command(pass_context=True)
    async def myid(self, ctx, member: discord.Member = None):
        if member is None:
            await self.bot.say("Your id is " + ctx.message.author.id)
        else:
            await self.bot.say("His id is " + str(member.id))

    @commands.command(pass_context=True)
    async def xp(self, ctx, member: discord.Member = None):
        if member is None:
            await self.bot.say("You have: " + str(self.get_xp(ctx.message.author.id)) + " xp.")
        else:
            await self.bot.say("{} have: ".format(member.name) + str(self.get_xp(member.id)) + " xp.")

    @commands.command(pass_context=True)
    async def coins(self, ctx, member: discord.Member = None):
        if member is None:
            await self.bot.say("You have: " + str(self.get_coin(ctx.message.author.id, 1)) + " Gold GDC and " + str(
                self.get_coin(ctx.message.author.id, 2)) + " Silver GDC.")
        else:
            await self.bot.say(
                "{} have: ".format(member.name) + str(self.get_coin(member.id, 1)) + " Gold GDC and " + str(
                    self.get_coin(member.id, 2)) + " Silver GDC.")

    @commands.command(pass_context=True)
    async def coin(self, ctx, member: discord.Member = None):
        if member is None:
            await self.bot.say("You have: " + str(self.get_coin(ctx.message.author.id, 1)) + " Gold GDC and " + str(
                self.get_coin(ctx.message.author.id, 2)) + " Silver GDC.")
        else:
            await self.bot.say(
                "{} have: ".format(member.name) + str(self.get_coin(member.id, 1)) + " Gold GDC and " + str(
                    self.get_coin(member.id, 2)) + " Silver GDC.")

    @commands.command(pass_context=True)
    async def leaderboard(self, ctx, item: str = 'Gold'):
        if item.upper() == 'GOLD':
            item = 1
            res1 = c.execute("SELECT Coins FROM Users ORDER BY Coins DESC;")
            res1 = res1.fetchall()
            embed = discord.Embed(title="Gold OGC Leaderboard", description="The top users with the most Gold OGC.",
                                  color=discord.Color.blue())
            res2 = c.execute("SELECT UserID FROM Users ORDER BY Coins DESC;")
            res2 = res2.fetchall()

        elif item.upper() == 'SILVER':
            item = 2
            res1 = c.execute("SELECT SecondCoins FROM Users ORDER BY SecondCoins DESC;")
            res1 = res1.fetchall()
            embed = discord.Embed(title="Silver OGC Leaderboard", description="The top users with the most Silver OGC.",
                                  color=discord.Color.blue())
            res2 = c.execute("SELECT UserID FROM Users ORDER BY SecondCoins DESC;")
            res2 = res2.fetchall()

        elif item.upper() == 'XP':
            item = 3
            res1 = c.execute("SELECT Xp FROM Users ORDER BY Xp DESC;")
            res1 = res1.fetchall()
            res2 = c.execute("SELECT UserID FROM Users ORDER BY Xp DESC;")
            res2 = res2.fetchall()

        if item == 1 or item == 2:
            for counter in range(0, 8):
                for member in ctx.message.server.members:
                    counter_id = str(res2[counter])[2:-3]
                    if counter_id == member.id:
                        embed.add_field(name=member.display_name, value=str(res1[counter])[1:-4], inline=False)
                        break
            await self.bot.say(embed=embed)
        elif item == 3:
            embed = discord.Embed(title="XP Leaderboard", description="The top users with the most XP.",
                                  color=discord.Color.blue())
            for counter in range(0, 8):
                for member in ctx.message.server.members:
                    counter_id = str(res2[counter])[2:-3]
                    if counter_id == member.id:
                        embed.add_field(name=member.display_name, value=str(res1[counter])[1:-2], inline=False)
                        break
            await self.bot.say(embed=embed)





    @commands.command(pass_context=True)
    async def pay(self, ctx, member: discord.Member = None, amount: int = None, the_type: str = None):
        if member is None:
            await self.bot.say("Hey, you can't pay air. Duh!")
        if amount is None:
            await self.bot.say("Yo, you need to tell me how much you're gonna pay " + member.name)
        elif the_type is None:
            await self.bot.say(
                "Maybe paying in a currency is better? Do you pay with hairs, air, or maybe Gold and Silver GDC?" + member.name)
        else:
            if the_type == "gold":
                the_type = 1
            elif the_type == "silver":
                the_type = 2
            author_coins = self.get_coin(ctx.message.author.id, the_type)
            if author_coins is not None:
                if author_coins < amount:
                    await self.bot.say("You don't have that much money")
                else:
                    self.remove_coin(ctx.message.author.id, amount, the_type)
                    self.add_coin(member.id, amount, the_type)
                    await self.bot.say("You successfully sent money to " + member.name)
            else:
                await self.bot.say("Please choose a real currency, like gold or silver.")

    @commands.cooldown(1, 86400, commands.BucketType.user)
    @commands.command(pass_context=True)
    async def daily(self, ctx):
        gold = random.randint(1, 50)
        silver = random.randint(1, 60)
        self.add_coin(ctx.message.author.id, gold, 1)
        self.add_coin(ctx.message.author.id, silver, 2)
        embed = discord.Embed(title="Daily", description="You received {} Gold OGC and {} Silver OGC".format(gold, silver))
        await self.bot.say(embed=embed)



    @commands.command(pass_context=True)
    async def flipcoin(self, ctx, amount: int = None, the_type: str = None):
        if amount is None:
            # await Currency.bot.say(
            #    "To bet money please enter an amount and the currency you want to deal with: gold or silver")
            # there's no error, this message would just confuse the user
            coin_side = random.choice(["Heads", "Tails"])
            await self.bot.send_message(ctx.message.channel, "Flipped a coin... " + coin_side)
        elif the_type != "gold" and the_type != "silver":
            await self.bot.send_message(ctx.message.channel, "Please set a currency, either gold or silver.")
        else:
            if the_type == "gold":
                the_type = 1
                to_save = "gold"
            elif the_type == "silver":
                the_type = 2
                to_save = "silver"
            else:
                the_type = None
                to_save = ""
            author_coins = self.get_coin(ctx.message.author.id, the_type)
            if author_coins <= amount + (int(amount / 2)):
                await self.bot.send_message(ctx.message.channel,
                                            "You don't have that much money, You cant bet all of your coins. And you need to have at least 1.5x of what you bet")
            else:
                if the_type == 1:
                    self.remove_coin(ctx.message.author.id, amount, 1)
                    coin_money_side = random.choice([1, 2])
                    if coin_money_side is 1:
                        win = amount * 2
                        self.add_coin(ctx.message.author.id, win, 1)
                        if the_type == 1:
                            gold_silver = 'Gold OGC'
                        elif the_type == 2:
                            gold_silver = 'Silver OGC'
                        else:
                            gold_silver = 'Bruh'
                        await self.bot.send_message(ctx.message.channel, "Heads.... You won " + str(win) + gold_silver)
                    elif coin_money_side == 2:
                        lose = amount + (int(amount / 2))
                        await self.bot.send_message(ctx.message.channel, "Tails.... Sorry, better luck next time?")
                        self.remove_coin(ctx.message.author.id, lose, 1)
                elif the_type == 2:
                    self.remove_coin(ctx.message.author.id, amount, 2)
                    coin_money_side = random.choice([1, 2])
                    if coin_money_side == 1:
                        win = amount * 2
                        self.add_coin(ctx.message.author.id, win, 2)
                        if the_type == 1:
                            gold_silver = 'Gold OGC'
                        elif the_type == 2:
                            gold_silver = 'Silver OGC'
                        else:
                            gold_silver = 'Bruh'
                        await self.bot.send_message(ctx.message.channel, "Heads.... You won " + str(win) + gold_silver)
                    elif coin_money_side == 2:
                        lose = amount + (int(amount / 2))
                        await self.bot.send_message(ctx.message.channel, "Tails.... Sorry, better luck next time?")
                        self.remove_coin(ctx.message.author.id, lose, the_type)

    @commands.command(pass_context=True)
    async def market(self, ctx):
        embed = discord.Embed(title="Stock Market", description="Actual value of currencies", color=0x0000ff)
        embed.set_author(name="OwGN-Helper", url="https://discord.gg/QVxwatk",
                         icon_url="https://cdn.discordapp.com/avatars/428560104321712158/5f2b8790c4f96a4ee158cdcc42aa15ac.webp?size=1024")
        embed.add_field(name="Gold OGC:",
                        value=str(self.get_rate(self.get_coins_number(1), self.get_coins_number(0))) + " Base OGC",
                        inline=False)
        embed.add_field(name="Silver OGC",
                        value=str(self.get_rate(self.get_coins_number(2), self.get_coins_number(0))) + " Base OGC",
                        inline=False)
        embed.add_field(name="Base OGC (Reference)", value="1, obviously", inline=False)
        embed.set_footer(text="Made by BluePhoenixGame, Stock Exchange by ClemCa")
        await self.bot.send_message(ctx.message.channel, embed=embed)

    @commands.command(pass_context=True)
    async def Exchange(self, ctx, amount: int = None, first_type: str = None, second_type: str = None):
        if amount is None:
            await self.bot.send_message(ctx.message.channel,
                                        "You successfully exchanged... Nothing! With... Nothing too! Excellent, no?")
        elif first_type is None:
            await self.bot.send_message(ctx.message.channel, "So what will you exchange in the end?")
        elif second_type is None:
            await self.bot.send_message(ctx.message.channel, "So what will you exchange in the end?")
        else:
            if first_type.upper() == "GOLD":
                first_type = 1
                first_str_type = 'Gold'
            elif first_type.upper() == "SILVER":
                first_type = 2
                first_str_type = 'Silver'
            else:
                await self.bot.say("There is no currency named {} choose between gold or silver.".format(first_type))
                return 0
            if second_type.upper() == "GOLD":
                second_type = 1
                second_str_type = 'Gold'
            elif second_type.upper() == "SILVER":
                second_type = 2
                second_str_type = 'Silver'
            else:
                await self.bot.say("There is no currency named {} choose between gold or silver.".format(second_type))
                return 0
            rate = self.get_rate(self.get_coins_number(first_type), self.get_coins_number(second_type))
            if rate > 0:
                toadd = amount * rate
                if toadd > 50:
                    toadd = toadd - (toadd * 0.1 / 100)
                else:
                    toadd = toadd - (toadd * 2 / 100)
                self.remove_coin(ctx.message.author.id, amount, first_type)
                self.add_coin(ctx.message.author.id, toadd, second_type)
                await self.bot.send_message(ctx.message.channel,
                                            "Exchanged {} {} to {}".format(str(amount), first_str_type,
                                                                           second_str_type))
            elif rate == "even":
                toadd = amount
                if toadd > 50:
                    toadd = toadd - (toadd * 0.1 / 100)
                else:
                    toadd = toadd - (toadd * 2 / 100)
                self.remove_coin(ctx.message.author.id, amount, first_type)
                self.add_coin(ctx.message.author.id, toadd, second_type)

                await self.bot.send_message(ctx.message.channel,
                                            "Exchanged {} {} to {}".format(str(amount), first_str_type,
                                                                           second_str_type))
            else:
                await self.bot.send_message(ctx.message.channel,
                                            "The value of the first currency is too low compared to the value of the second. Impossible to exchange.")

    async def on_member_join(self, member):
        self.create_user_if_not_exists(member.id)

    async def on_message(self, message):
        self.add_xp(message.author.id, 1)
        should_get_coin = random.choice([True, False])
        # chances = 50#+(CalculationOfBonuses) ^As there aren't any bonuses here (no party no group no guild
        # prepared), the calculation of bonuses will stay in comment even if the edits I did are ok
        # should_get_coin = (chances >= random.random*100)
        if should_get_coin is True:
            should_get_coin = random.choice([True, False])
            if should_get_coin is True:
                self.add_coin(message.author.id, 1, 1)
            else:
                self.add_coin(message.author.id, 1, 2)
        else:
            pass


def setup(bot):
    bot.add_cog(Currency(bot))
