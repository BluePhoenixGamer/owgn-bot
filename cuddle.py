import discord
from discord.ext import commands
import asyncio
import random


class Cuddle():
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def hug(self, ctx, *, member: discord.Member = None):
        await self.bot.delete_message(ctx.message)
        hugif = random.choice([
            "https://media1.tenor.com/images/87ce1545bd99ccb269005e3ad706f6d8/tenor.gif?itemid=7278312",
            "https://media1.tenor.com/images/79d425d4c76ac4cf5b422f764453bfee/tenor.gif?itemid=5026053",
            "https://media1.tenor.com/images/68a86beda361994b2dd38ade6a252602/tenor.gif?itemid=3904776",
            "https://media1.tenor.com/images/4dc06c2c02cfd3071533726251587160/tenor.gif?itemid=10235333",
            "https://media1.tenor.com/images/cb3031d8f1fb32d097ec899592ab7eec/tenor.gif?itemid=5406385",
            "https://media1.tenor.com/images/a8ec886e2e1bfd28aeabb1825d33622f/tenor.gif?itemid=9512793",
            "https://media1.tenor.com/images/e46e26b2e3c312bfdf4d229ec1fff53f/tenor.gif?itemid=4272550",
            "https://media1.tenor.com/images/ccf75a83970b7b858e8826459156dba9/tenor.gif?itemid=11701486",
            "https://media1.tenor.com/images/42922e87b3ec288b11f59ba7f3cc6393/tenor.gif?itemid=5634630",
            "https://media1.tenor.com/images/e58eb2794ff1a12315665c28d5bc3f5e/tenor.gif?itemid=10195705"
        ])
        member_name = member.display_name
        author_name = ctx.message.author.display_name
        if member is None:
            bot_error_hug = await self.bot.say(":thinking:I'm confused, who will you hug? :thinking:")
            await asyncio.sleep(10)
            await self.bot.delete_message(bot_error_hug)
        else:
            if member.id == ctx.message.author.id:
                bot_themselves_hug = await self.bot.say(
                    ctx.message.author.mention + " Have no one to hug and therefor hugged themselves")
                await asyncio.sleep(15)
                await self.bot.delete_message(bot_themselves_hug)
            else:
                em = discord.Embed(title=member_name + " was hugged by " + author_name, image=hugif)
                em.set_image(url=hugif)
                await self.bot.say(embed=em)

    @commands.command(pass_context=True)
    async def kiss(self, ctx, *, member: discord.Member = None):
        kissgif = random.choice(
            ["https://media1.tenor.com/images/cddc857b201c0273452a7ceddf0acca4/tenor.gif?itemid=9836808",
             "https://media1.tenor.com/images/fb92d0be78a1ea19af0168c0ca29c1bd/tenor.gif?itemid=5615952",
             "https://media1.tenor.com/images/2f4fef2f1e565ea2541c693a25e4bdfc/tenor.gif?itemid=4531955",
             "https://media1.tenor.com/images/8438e6772a148e62f4c64332ea7da9e8/tenor.gif?itemid=5628024",
             "https://media1.tenor.com/images/7a564f6f9d28fce1be20982af38a7e7a/tenor.gif?itemid=5043393",
             "https://media1.tenor.com/images/1b6fd95b758f1a4d43be0f4a05ce98ec/tenor.gif?itemid=5090086",
             "https://media1.tenor.com/images/2f7cd6856ed5ddeb6a17cf312e29c6a2/tenor.gif?itemid=5504953",
             "https://media1.tenor.com/images/a66f4ac0deded0a2a12260cb1af11c3c/tenor.gif?itemid=11034277",
             "https://media1.tenor.com/images/1aa9d2694e114dda6ea97bc3486d43a2/tenor.gif?itemid=3573784",
             "https://media1.tenor.com/images/4bfe919c546a0d8f854c951dd7cfa823/tenor.gif?itemid=3450888"])
        member_name = member.display_name
        author_name = ctx.message.author.display_name
        if member is None:
            bot_error_hug = await self.bot.say(":thinking: I'm confused, who will you kiss? :thinking:")
            await asyncio.sleep(10)
            await self.bot.delete_message(bot_error_hug)
        else:
            if member.id == ctx.message.author.id:
                bot_themselves_hug = await self.bot.say(ctx.message.author.mention + " Tried to kiss themselves.")
                await asyncio.sleep(15)
                await self.bot.delete_message(bot_themselves_hug)
            else:
                em = discord.Embed(title=member_name + " was kissed by " + author_name, image=kissgif)
                em.set_image(url=kissgif)
                await self.bot.say(embed=em)


def setup(bot):
    bot.add_cog(Cuddle(bot))
