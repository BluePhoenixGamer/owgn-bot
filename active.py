import discord
from discord.ext import commands
import sqlite3
import asyncio
import datetime

conn = sqlite3.connect('users.db', isolation_level=None)
c = conn.cursor()
c.execute("ATTACH DATABASE 'rpg.db' AS RPG")
c.execute("ATTACH DATABASE 'shop.db' AS SHOP")
c.execute("ATTACH DATABASE 'settings.db' AS SETTINGS")
c.execute("""CREATE TABLE IF NOT EXISTS Users(
                      UserID TEXT,
                      Coins FLOAT,
                      SecondCoins FLOAT,
                      Xp INTEGER,
                      Online TEXT,
                      Party TEXT,
                      MyGroup TEXT,
                      Guild TEXT,
                      PRIMARY KEY(UserID))""")
c.execute("""CREATE TABLE IF NOT EXISTS RPG.Inventory(
                      UserID TEXT,
                      Type TEXT,
                      Name TEXT,
                      Quantity INT)""")
c.execute("""CREATE TABLE IF NOT EXISTS SHOP.Shop(
                      UserID TEXT,
                      Item TEXT,
                      Type TEXT,
                      Price FLOAT,
                      Sold TEXT)""")

server_id = "422795900717367296"

owgn = "None"

class Active:
    def __init__(self, bot):
        self.bot = bot
        self.bot.loop.create_task(self.active_check())

    def active_update(self, userid):
        prevision = datetime.date.today() + datetime.timedelta(days=30)
        res = c.execute("SELECT Online FROM Users WHERE UserID=?", (userid,))
        res = res.fetchone()
        if res is None:
            pass
        else:
            c.execute("UPDATE Users SET Online=? WHERE UserID=?", (prevision, userid))
            conn.commit()
        # After thinking about it a bit, storing a prevision is easier than to store today's date from a readability
        # point of view

    async def active_check(self):
        today = datetime.date.today()
        global owgn
        if owgn == "None":
            for server in self.bot.servers:
                if server.id == server_id:
                    owgn = server
                    break
        else:
            for member in owgn.members:
                res = c.execute("SELECT Online From Users WHERE UserID=?", (member.id,))
                res = res.fetchone()
                if res is None:
                    self.active_update(member.id)
                    pass
                else:
                    if today == res:
                        self.bot.send_message(member, "You havent been active for 30 days, Therefore you were kicked.\nIf you were in holidays or had some IRL problems and are now crying over this message, then you are welcome!\nHere's the invite: https://discord.gg/sjH5WkP")
                        self.bot.kick(member)
            await asyncio.sleep(86400)

    async def on_message(self, message):
        self.active_update(message.author.id)

    async def on_message_edit(self, before, after):
        self.active_update(after.author.id)

    async def on_member_join(self, member):
        asyncio.sleep(2)
        self.active_update(member.id)

    async def on_member_update(self, before, after):
        self.active_update(after.id)

    async def on_reaction_add(self, reaction, user):
        self.active_update(user.id)

    async def on_reaction_remove(self, reaction, user):
        self.active_update(user.id)

    async def on_voice_state_update(self, before, after):
        self.active_update(after.id)

    async def on_typing(self, channel, user, when):
        self.active_update(user.id)

def setup(bot):
    bot.add_cog(Active(bot))